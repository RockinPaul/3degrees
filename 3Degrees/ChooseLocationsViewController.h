//
//  ChooseLocationsViewController.h
//  3Degrees
//
//  Created by Pavel on 22.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationCell.h"

@interface ChooseLocationsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *locationsArray;
@property (nonatomic, strong) NSMutableArray *selectedLocationsIDs;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *heightConstraint; // 44 by default. Table bottom constraint.
@property (nonatomic) float tableViewHeight;
@property (nonatomic) BOOL isOpen;

- (IBAction)openButtonPressed:(UIButton *)sender;
- (IBAction)backButtonPressed:(UIButton *)sender;

@end
