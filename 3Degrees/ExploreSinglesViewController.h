//
//  ExploreSinglesViewController.h
//  3Degrees
//
//  Created by Pavel on 12.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExploreCollectionViewCell.h"
#import "iCarousel.h"
#import "FilterViewController.h"

@interface ExploreSinglesViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, strong) IBOutlet iCarousel *topCarousel;
@property (nonatomic, strong) IBOutlet iCarousel *bottomCarousel;
@property (nonatomic, strong) IBOutlet UILabel *positionLabel;
@property (nonatomic, strong) IBOutlet UILabel *jobLabel;
@property (nonatomic, strong) IBOutlet UILabel *degreeLabel;
@property (nonatomic, strong) IBOutlet UILabel *institutionLabel;
@property (nonatomic, strong) IBOutlet UITextView *bioTextView;

@property (nonatomic, strong) NSArray *singlesArray;
@property (nonatomic, strong) NSArray *ownSinglesArray;
@property (nonatomic, strong) NSMutableArray *usernamesArray;
@property (nonatomic, strong) NSMutableArray *ownUsernamesArray;
@property (nonatomic, strong) NSMutableArray *photosArray;
@property (nonatomic, strong) NSMutableArray *ownPhotosArray;
@property (nonatomic, strong) NSMutableArray *agesArray;
@property (nonatomic, strong) NSMutableArray *ownAgesArray;
@property (nonatomic, strong) NSMutableArray *locationsArray;
@property (nonatomic, strong) NSMutableArray *ownLocationsArray;
@property (nonatomic, strong) NSMutableArray *singlesIDsArray;
@property (nonatomic, strong) NSMutableArray *ownSinglesIDsArray;
@property (nonatomic) BOOL isInitialCase;
@property (nonatomic, strong) NSString *currentSingleId;
@property (nonatomic, strong) NSString *currentOwnSingleId;

- (IBAction)pairButtonPressed:(UIButton *)sender;

@end
