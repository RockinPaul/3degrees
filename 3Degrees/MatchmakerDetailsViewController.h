//
//  MatchmakerDetailsViewController.h
//  3Degrees
//
//  Created by Pavel on 17.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchmakerCell.h"
#import "MyNetworkCell.h"
#import "Single.h"

@interface MatchmakerDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *singlesArray;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userDescription;
@property (nonatomic, strong) UIImage *coverImage;

- (IBAction)backButtonPressed:(UIButton *)sender;

@end
