//
//  ChooseDatesViewController.m
//  3Degrees
//
//  Created by Pavel on 22.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ChooseDatesViewController.h"

@implementation ChooseDatesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - PDTSimpleCalendarViewDelegate

- (void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date
{
    NSLog(@"Date Selected : %@", date);
    NSLog(@"Date Selected with Locale %@", [date descriptionWithLocale:[NSLocale systemLocale]]);
}

- (BOOL)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller shouldUseCustomColorsForDate:(NSDate *)date
{
    if ([self.customDates containsObject:date]) {
        return YES;
    }
    
    return NO;
}

- (UIColor *)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller circleColorForDate:(NSDate *)date
{
    return [UIColor whiteColor];
}

- (UIColor *)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller textColorForDate:(NSDate *)date
{
    return [UIColor orangeColor];
}

#pragma mark - Private

//Add 3 custom dates, the 15th for the current & 2 next months.
- (void)initCustomDates
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:[NSDate date]];
    components.day = 15;
    NSDate *date1 = [[NSCalendar currentCalendar] dateFromComponents:components];
    NSDateComponents *addOneMonthComponents = [[NSDateComponents alloc] init];
    addOneMonthComponents.month =1;
    NSDate *date2 = [[NSCalendar currentCalendar] dateByAddingComponents:addOneMonthComponents toDate:date1 options:0];
    NSDate *date3 = [[NSCalendar currentCalendar] dateByAddingComponents:addOneMonthComponents toDate:date2 options:0];
    self.customDates = @[date1, date2, date3];
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString *segueIdentifier = segue.identifier;
    if ([segueIdentifier isEqualToString:@"DateContainer"]) {
        
        [self initCustomDates];
        DateContainerViewController *calendarViewController = [segue destinationViewController];
        //This is the default behavior, will display a full year starting the first of the current month
        [calendarViewController setDelegate:self];
        calendarViewController.weekdayHeaderEnabled = NO;
        calendarViewController.weekdayTextType = PDTSimpleCalendarViewWeekdayTextTypeVeryShort;
}

// Get the new view controller using [segue destinationViewController].
// Pass the selected object to the new view controller.
}


@end
