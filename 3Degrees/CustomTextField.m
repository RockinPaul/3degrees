//
//  CustomTextField.m
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField


//place holder position

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10, 4);
}

// text position
- (CGRect)textRectForBounds:(CGRect)bounds {
    
    return CGRectInset(bounds, 10,4);
}

// text position while editing
- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    return CGRectInset(bounds, 10, 4);
}


@end
