//
//  ChooseDatesViewController.h
//  3Degrees
//
//  Created by Pavel on 22.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PDTSimpleCalendarViewController.h"
#import "PDTSimpleCalendarViewCell.h"
#import "DateContainerViewController.h"

@interface ChooseDatesViewController : UIViewController <PDTSimpleCalendarViewDelegate>

@property (nonatomic, strong) NSArray *customDates;

- (IBAction)backButtonPressed:(UIButton *)sender;

@end
