//
//  ModeSelectionViewController.h
//  3Degrees
//
//  Created by Pavel on 13.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface ModeSelectionViewController : ViewController

@property (nonatomic, strong) IBOutlet UILabel *pleaseLabel;
@property (nonatomic, strong) IBOutlet UIButton *matchmakerButton;
@property (nonatomic, strong) IBOutlet UIButton *singleButton;
@property (nonatomic, strong) IBOutlet UIButton *nextButton;
@property (nonatomic) BOOL isSingle;

- (IBAction)matchmakerButtonPressed:(id)sender;
- (IBAction)singleButtonPressed:(id)sender;
- (IBAction)nextButtonPressed:(id)sender;

@end
