//
//  InviteViewController.m
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "InviteViewController.h"

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
#warning test mode
    self.contactsArray = @[@"Adam Smith", @"Connie Grayson", @"Elena Arroy", @"Fulvio Pozzobon", @"Florence Roumieu", @"Geoff Hubbard", @"Gregor Telfer", @"James Fennessy", @"Kevin Westley", @"Lorie Gibson", @"Nilo Otero", @"Shelly Goldstack"];
    self.sentArray = [NSMutableArray new];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *emails = [defaults objectForKey:@"emails"];
    self.contactsArray = [emails allValues];
    self.emailsArray = [emails allKeys];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textChangeNotification:) name:UITextFieldTextDidChangeNotification object:nil];
    self.searchContactsArray = [[NSMutableArray alloc] initWithArray:self.contactsArray];
    self.searchEmailsArray = [[NSMutableArray alloc] initWithArray:self.emailsArray];
}

-(void)textChangeNotification:(NSNotification *)notification {
    [self searchRecordsAsPerText:self.searchTextField.text];
}

-(void)searchRecordsAsPerText:(NSString *)string
{
    [self.searchContactsArray removeAllObjects];
    [self.searchEmailsArray removeAllObjects];

    for (int i = 0; i < self.contactsArray.count; i++) {
        NSString *contact = self.contactsArray[i];
        NSString *email = self.emailsArray[i];
        NSRange titleResultsRange = [contact rangeOfString:string options:NSCaseInsensitiveSearch];
        
        if (titleResultsRange.length > 0)
        {
            [self.searchContactsArray addObject:contact];
            [self.searchEmailsArray addObject:email];
        }

    }
    [self.tableView reloadData];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"Cell";
    ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[ContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
#warning test mode
    cell.usernameLabel.text = self.searchContactsArray[indexPath.row];
    cell.phoneLabel.text = self.searchEmailsArray[indexPath.row];

    if ([self.sentArray containsObject:indexPath]) {
        cell.sendButton.titleLabel.text = @"Sent";
    }
    
    [cell.sendButton addTarget:self action:@selector(sendButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchContactsArray.count;
}

- (void)backButtonPressed:(id)sender {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

#warning send/sent issue

- (void)sendButtonPressed:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil) {
        [self.sentArray addObject:indexPath];
        [self.tableView reloadData];
    }
    
    SEL inviteSelector = @selector(inviteUserWithParameters:);
    NSString *bio = self.textView.text;
    NSString *email = self.searchEmailsArray[indexPath.row];
    NSString *role = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userRole = [defaults stringForKey:@"role"];
    if ([userRole isEqualToString:@"matchmaker"]) {
        role = @"single";
    } else if ([userRole isEqualToString:@"single"]){
        role = @"matchmaker";
    } else {
        role = @"";
        NSLog(@"ERROR: User role undefined");
    }
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjects:@[email, role, bio] forKeys:@[@"email", @"role", @"bio"]];
    [RESThelper performRequest:inviteSelector ForTarget:self WithParameters:parameters];
}

- (void)dismissKeyboard {
    if ([self.textView isFirstResponder] || [self.searchTextField isFirstResponder]) {
        [self.textView resignFirstResponder];
        [self.searchTextField resignFirstResponder];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.searchTextField resignFirstResponder];
    return YES;
}

#pragma mark - Send invite
#pragma mark - Wrapper

// Wrapper for selector
- (void)inviteUserWithParameters:(NSDictionary *)params {
    NSString *email = [params valueForKey:@"email"];
    NSString *role = [params valueForKey:@"role"];
    NSString *bio = [params valueForKey:@"bio"];
    [self inviteUser:email withRole:role AndBio:bio];
}

#pragma mark - Matchmakers list with limit/offset

- (void)inviteUser:(NSString *)email
          withRole:(NSString *)role
            AndBio:(NSString *)bio {
    
    NSLog(@"%@ - EMAIL", email);
    NSLog(@"%@ - ROLE", role);
    NSLog(@"%@ - BIO", bio);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/invite?access_token=%@", base_url, [defaults stringForKey:@"access_token"]];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager POST:completeURL parameters:@{@"email": email,
                                          @"role": role,
                                          @"bio": bio
                                          }
         progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSLog(@"%@", responseObject);
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

@end
