//
//  ActivityViewController.h
//  3Degrees
//
//  Created by Pavel on 28.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "MyNetworkViewController.h"
#import "ProposedCell.h"
#import "RejectedCell.h"
#import "AcceptedCell.h"
#import "ScheduledCell.h"
#import "JoinRequestCell.h"
#import "Activity.h"
#import "ProfileViewController.h"

@interface ActivityViewController : MyNetworkViewController

@property (nonatomic, strong) NSArray *activityArray;

- (IBAction)profileButtonPressed:(id)sender;

@end
