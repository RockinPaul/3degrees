//
//  ChooseTimesViewController.h
//  3Degrees
//
//  Created by Pavel on 22.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageHandler.h"

@interface ChooseTimesViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *time1200Button;
@property (nonatomic, strong) IBOutlet UIButton *time1230Button;
@property (nonatomic, strong) IBOutlet UIButton *time100Button;
@property (nonatomic, strong) IBOutlet UIButton *time200Button;
@property (nonatomic, strong) IBOutlet UIButton *time230Button;
@property (nonatomic, strong) IBOutlet UIButton *time300Button;
@property (nonatomic, strong) IBOutlet UIButton *time330Button;
@property (nonatomic, strong) IBOutlet UIButton *time400Button;
@property (nonatomic, strong) IBOutlet UIButton *time430Button;
@property (nonatomic, strong) IBOutlet UIButton *time500Button;
@property (nonatomic, strong) IBOutlet UIButton *time530Button;
@property (nonatomic, strong) IBOutlet UIButton *time600Button;
@property (nonatomic, strong) IBOutlet UIButton *time630Button;
@property (nonatomic, strong) IBOutlet UIButton *time700Button;
@property (nonatomic, strong) IBOutlet UIButton *time730Button;
@property (nonatomic, strong) IBOutlet UIButton *time800Button;
@property (nonatomic, strong) IBOutlet UIButton *time830Button;
@property (nonatomic, strong) IBOutlet UIButton *time900Button;
@property (nonatomic, strong) IBOutlet UIButton *time930Button;
@property (nonatomic, strong) IBOutlet UIButton *time1000Button;
@property (nonatomic, strong) IBOutlet UIButton *time1030Button;

@property (nonatomic, strong) NSMutableArray *selectedTimesArray;

- (IBAction)backButtonPressed:(UIButton *)sender;
- (IBAction)timeButtonPressed:(UIButton *)sender;

@end
