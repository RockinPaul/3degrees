//
//  MyNetworkViewController.h
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyNetworkCell.h"
#import "AddHeaderCell.h"
#import <AFNetworking.h>
#import "RESThelper.h"
#import "Constants.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "ImageHandler.h"
#import "Single.h"
#import "Matchmaker.h"
#import "MatchmakerDetailsViewController.h"
#import "SingleDetailsViewController.h"

@interface MyNetworkViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *singlesButton;
@property (nonatomic, strong) IBOutlet UIButton *matchmakersButton;
@property (nonatomic, strong) NSArray *networkArray;
@property (nonatomic, strong) NSArray *singlesArray;
@property (nonatomic, strong) NSArray *matchmakersArray;
@property (nonatomic) BOOL isSingles;

- (IBAction)addButtonPressed:(id)sender;
- (IBAction)singlesButtonPressed:(id)sender;
- (IBAction)matchmakersButtonPressed:(id)sender;

@end
