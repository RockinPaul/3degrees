
//
//  AddHeaderCell.m
//  3Degrees
//
//  Created by Pavel on 28.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "AddHeaderCell.h"

@implementation AddHeaderCell

- (void)awakeFromNib {
    [self addSubview:self.titleButton];
}

@end
