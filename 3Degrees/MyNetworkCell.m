//
//  MyNetworkCell.m
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "MyNetworkCell.h"

@implementation MyNetworkCell

- (void)awakeFromNib {    
    [self addSubview:self.avatarImageView];
    [self addSubview:self.usernameLabel];
    [self addSubview:self.mmCountLabel];
}

@end
