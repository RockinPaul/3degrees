//
//  ModeSelectionViewController.m
//  3Degrees
//
//  Created by Pavel on 13.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ModeSelectionViewController.h"

@class ViewController;

@implementation ModeSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pleaseLabel.alpha = 0.75;
    self.nextButton.alpha = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(popBack)
                                                 name:@"logout"
                                               object:nil];
}

- (void)popBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)matchmakerButtonPressed:(id)sender {
    [self.matchmakerButton setBackgroundImage:[UIImage imageNamed:@"button_pink"] forState:UIControlStateNormal];
    [self.singleButton setBackgroundImage:[UIImage imageNamed:@"button_pink_opacity"] forState:UIControlStateNormal];
    self.nextButton.alpha = 1;
    self.isSingle = NO;
}

- (void)singleButtonPressed:(id)sender {
    [self.matchmakerButton setBackgroundImage:[UIImage imageNamed:@"button_pink_opacity"] forState:UIControlStateNormal];
    [self.singleButton setBackgroundImage:[UIImage imageNamed:@"button_pink"] forState:UIControlStateNormal];
    self.nextButton.alpha = 1;
    self.isSingle = YES;
}

- (void)nextButtonPressed:(id)sender {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (self.isSingle) {
        [defaults setObject:@"single" forKey:@"role"];
    } else {
        [defaults setObject:@"matchmaker" forKey:@"role"];
    }
    [defaults synchronize];
    
    NSString *completeURL = [NSString stringWithFormat:@"%@db/user/role?access_token=%@", base_url, [defaults stringForKey:@"access_token"]];
    
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager PUT:completeURL parameters:@{@"role": [defaults stringForKey:@"role"]}
          success:^(NSURLSessionTask *task, id responseObject) {
              NSLog(@"JSON: %@", responseObject);
              
              TabBarController *tabBarController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarController"];
              [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:tabBarController animated:YES completion:nil];
              
          } failure:^(NSURLSessionTask *operation, NSError *error) {
              [RESThelper errorDescription:error];
          }];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
