//
//  LocationCell.h
//  3Degrees
//
//  Created by Pavel on 22.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *locationLabel;

@end
