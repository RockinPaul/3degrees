//
//  SchedulingViewController.h
//  3Degrees
//
//  Created by Pavel on 26.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <AFNetworking.h>
#import "RESThelper.h"
#import "ChooseDatesViewController.h"
#import "ChooseTimesViewController.h"
#import "ChooseLocationsViewController.h"

@interface SchedulingViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *dateView;
@property (nonatomic, strong) IBOutlet UIView *timeView;
@property (nonatomic, strong) IBOutlet UIView *locationView;

@property (nonatomic, strong) NSArray *locationsArray;

@end
