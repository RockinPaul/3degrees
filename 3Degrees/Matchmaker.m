//
//  Matchmaker.m
//  3Degrees
//
//  Created by Pavel on 13.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "Matchmaker.h"

@implementation Matchmaker

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.countSingle forKey:@"countSingle"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]){
        self.countSingle = [aDecoder decodeObjectForKey:@"countSingle"];
    }
    return self;
}

@end
