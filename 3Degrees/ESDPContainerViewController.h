//
//  ESDPContainerViewController.h
//  3Degrees
//
//  Created by Pavel on 14.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESDPContainerViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *proposalsContainer;
@property (nonatomic, strong) IBOutlet UIView *exploreContainer;

@end
