//
//  DateProposalsViewController.h
//  3Degrees
//
//  Created by Pavel on 28.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyNetworkViewController.h"
#import "ProfileViewController.h"

@interface DateProposalsViewController : MyNetworkViewController <UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UITextView *textView;

- (IBAction)profileButtonPressed:(id)sender;

@end
