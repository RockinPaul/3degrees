//
//  MatchmakerDetailsViewController.m
//  3Degrees
//
//  Created by Pavel on 17.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "MatchmakerDetailsViewController.h"

@interface MatchmakerDetailsViewController ()

@end

@implementation MatchmakerDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@", self.coverImage);
    // Do any additional setup after loading the view.
}


#pragma mark - Cell for row at indexPath

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = nil;
    if (indexPath.row > 0) {
        cellIdentifier = @"MatchmakerNetwork";
        MyNetworkCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        Single *single = self.singlesArray[indexPath.row];
        cell.usernameLabel.text = [NSString stringWithFormat:@"%@ %@", single.firstName, single.lastName];
        cell.mmCountLabel.text = single.userDescription;
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:single.imageLink]];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            cell.avatarImageView.image = image;
        }
        
        return cell;
    } else {
        cellIdentifier = @"Matchmaker";
        MatchmakerCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.coverImageView.image = [UIImage imageNamed:@"blank_explore"];
        if (self.coverImage != nil) {
            cell.coverImageView.image = self.coverImage;
        }
        cell.userNameLabel.text = self.userName;
        cell.descriptionLabel.text = self.userDescription;
        return cell;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.singlesArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 0) {
        return 71.0;
    } else {
        return 227.0;
    }
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
