//
//  AddHeaderCell.h
//  3Degrees
//
//  Created by Pavel on 28.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddHeaderCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIButton *titleButton;

@end
