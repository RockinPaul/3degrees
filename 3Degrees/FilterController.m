//
//  FilterController.m
//  3Degrees
//
//  Created by Pavel on 11.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "FilterController.h"

@implementation FilterController

#pragma mark - Apply filter

- (void)performRequestForSinglesWithParameters:(NSDictionary *)params {
//    SEL selector = @selector(applyFilterWithParameters:);
//    [RESThelper performRequest:selector ForTarget:self WithParameters:params];
    [self applyFilterWithParameters:params];
}

- (void)applyFilterWithParameters:(NSDictionary *)parameters {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults stringForKey:@"access_token"];
    
    NSString *ageMin = [parameters valueForKey:@"ageMin"];
    NSString *ageMax = [parameters valueForKey:@"ageMax"];
    NSString *distance = [parameters valueForKey:@"distance"];
    NSString *sex = [parameters valueForKey:@"sex"];
    NSString *matchmakersId = [parameters valueForKey:@"matchmakersId"];
    NSString *offset = [parameters valueForKey:@"offset"];
    NSString *limit = [parameters valueForKey:@"limit"];
    
    NSString *completeURL = [NSString stringWithFormat:@"%@search/user?access_token=%@&&", base_url, accessToken];
    
    if (parameters != nil) {
        if (![ageMin isEqualToString:@""] && ![ageMax isEqualToString:@""]) {
            completeURL = [completeURL stringByAppendingString:[NSString stringWithFormat:@"ageMin=%@&&ageMax=%@&&", ageMin, ageMax]];
        }
        if (![distance isEqualToString:@""]) {
            completeURL = [completeURL stringByAppendingString:[NSString stringWithFormat:@"distance=%@&&", distance]];
        }
        if (![sex isEqualToString:@""]) {
            completeURL = [completeURL stringByAppendingString:[NSString stringWithFormat:@"sex=%@&&", sex]];
        }
        if (![matchmakersId isEqual:nil]) {
            completeURL = [completeURL stringByAppendingString:[NSString stringWithFormat:@"matchmakersId=%@&&", matchmakersId]];
        }
        if (![offset isEqualToString:@""]) {
            completeURL = [completeURL stringByAppendingString:[NSString stringWithFormat:@"offset=%@&&", offset]];
        }
        if (![limit isEqualToString:@""]) {
            completeURL = [completeURL stringByAppendingString:[NSString stringWithFormat:@"limit=%@", limit]];
        }
    }
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager GET:completeURL parameters:nil progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSMutableArray *array = [NSMutableArray new];
             Single *single = nil;
             self.singles = [NSArray new];

             for (NSDictionary *singleDict in responseData) {
                 single = [Single new];
                 single.firstName = [singleDict objectForKey:@"firstName"];
                 single.lastName = [singleDict objectForKey:@"lastName"];
                 single.email = [singleDict objectForKey:@"email"];
                 single.iD = [singleDict objectForKey:@"id"];
                 single.profession = [singleDict objectForKey:@"profession"];
                 single.job = [singleDict objectForKey:@"organization"];
                 single.imageLink = [singleDict objectForKey:@"image"];
                 single.sex = [singleDict objectForKey:@"sex"];
                 single.bio = [singleDict objectForKey:@"bio"];
                 single.location = [singleDict objectForKey:@"location"];
                 single.age = [singleDict objectForKey:@"age"];
                 NSDictionary *education = [singleDict objectForKey:@"education"];
                 single.institution = [education objectForKey:@"institution"];
                 NSDictionary *degree = [education objectForKey:@"degree"];
                 
                 if (![degree isEqual:(NSDictionary *)[NSNull null]]) {
                     single.degreeId = [degree objectForKey:@"id"];
                     single.degreeTitle = [degree objectForKey:@"title"];
                 }
                 [array addObject:single];
             }

             self.singles = array;
             // POST NOTIFICATION TO EXPLORE SINGLES VIEW CONTROLLER
             if (parameters == nil) {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"requestWithoutParameters"
                                                                     object:self];
             } else {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"applyFilterWithParameters"
                                                                     object:self];
             }
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

#pragma mark - Shared instance

+ (FilterController *)sharedInstance {
    static dispatch_once_t pred;
    static FilterController *sharedInstance = nil;
    dispatch_once(&pred, ^ {
        sharedInstance = [FilterController new];
        sharedInstance.applySelector = @selector(applyFilterWithParameters:);
    });
    return sharedInstance;
}

@end
