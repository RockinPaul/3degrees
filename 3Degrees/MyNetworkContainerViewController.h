//
//  MyNetworkContainerViewController.h
//  3Degrees
//
//  Created by Pavel on 13.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "MyNetworkViewController.h"

@interface MyNetworkContainerViewController : UIViewController

- (IBAction)profileButtonPressed:(id)sender;

@property (nonatomic, strong) NSArray *singlesArray;
@property (nonatomic, strong) NSArray *matchmakersArray;

@end
