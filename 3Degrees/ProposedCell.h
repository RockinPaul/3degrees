//
//  ProposedCell.h
//  3Degrees
//
//  Created by Pavel on 10.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProposedCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *singleImageView;
@property (nonatomic, strong) IBOutlet UILabel *singleName;
@property (nonatomic, strong) IBOutlet UILabel *singleDescription;
@property (nonatomic, strong) IBOutlet UILabel *text;
@property (nonatomic, strong) IBOutlet UILabel *matchmakerLabel;

@end
