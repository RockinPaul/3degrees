//
//  Single.m
//  3Degrees
//
//  Created by Pavel on 10.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "Single.h"

@implementation Single

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.iD forKey:@"iD"];
    [aCoder encodeObject:self.firstName forKey:@"firstName"];
    [aCoder encodeObject:self.lastName forKey:@"lastName"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.institution forKey:@"institution"];
    [aCoder encodeObject:self.degreeId forKey:@"degreeId"];
    [aCoder encodeObject:self.degreeTitle forKey:@"degreeTitle"];
    [aCoder encodeObject:self.imageLink forKey:@"imageLink"];
    [aCoder encodeObject:self.location forKey:@"location"];
    [aCoder encodeObject:self.looking forKey:@"looking"];
    [aCoder encodeObject:self.profession forKey:@"profession"];
    [aCoder encodeObject:self.sex forKey:@"sex"];
    [aCoder encodeObject:self.age forKey:@"age"];
    [aCoder encodeObject:self.bio forKey:@"bio"];
    [aCoder encodeObject:self.bio forKey:@"job"];
    [aCoder encodeObject:self.bio forKey:@"userDescription"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]){
        self.iD = [aDecoder decodeObjectForKey:@"iD"];
        self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
        self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.institution = [aDecoder decodeObjectForKey:@"institution"];
        self.degreeId = [aDecoder decodeObjectForKey:@"degreeId"];
        self.degreeTitle = [aDecoder decodeObjectForKey:@"degreeTitle"];
        self.imageLink = [aDecoder decodeObjectForKey:@"imageLink"];
        self.location = [aDecoder decodeObjectForKey:@"location"];
        self.looking = [aDecoder decodeObjectForKey:@"looking"];
        self.profession = [aDecoder decodeObjectForKey:@"profession"];
        self.sex = [aDecoder decodeObjectForKey:@"sex"];
        self.age = [aDecoder decodeObjectForKey:@"age"];
        self.bio = [aDecoder decodeObjectForKey:@"bio"];
        self.job = [aDecoder decodeObjectForKey:@"job"];
        self.userDescription = [aDecoder decodeObjectForKey:@"userDescription"];
    }
    return self;
}

@end
