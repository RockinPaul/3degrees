//
//  InviteViewController.h
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactCell.h"
#import <AFNetworking.h>
#import "Constants.h"
#import "RESThelper.h"

@interface InviteViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet UITextField *searchTextField;

@property (nonatomic, strong) NSArray *contactsArray;
@property (nonatomic, strong) NSArray *emailsArray;
@property (nonatomic, strong) NSMutableArray *sentArray;
@property (nonatomic, strong) NSMutableArray *searchContactsArray;
@property (nonatomic, strong) NSMutableArray *searchEmailsArray;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)sendButtonPressed:(id)sender;

@end
