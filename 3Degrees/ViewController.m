//
//  ViewController.m
//  3Degrees
//
//  Created by Pavel on 07.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "ViewController.h"

@class ModeSelectionViewController;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bottombackgroundView = [UIImageView new];
    [self.view addSubview:self.bottombackgroundView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(googleInfoReceived:)
                                                 name:@"google"
                                               object:nil];
    
    self.radioArray = @[self.radio1, self.radio2, self.radio3, self.radio4, self.radio5]; // for swipe gesture recognizer
    self.backgroundArray = @[@"auth_photo1", @"auth_photo2", @"auth_photo3", @"auth_photo4", @"auth_photo5"];
    [self radioButtonPressed:self.radio1];
    NSLog(@"%@: BACK BACK", self.backgroundArray);
    // Hide status bar
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        NSLog(@"iOS <= 6");
//        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    self.isAlreadyLogged = NO;
}

- (void)googleInfoReceived:(NSNotification *)notification {
    NSDictionary *params = [notification object];
    [self logInBySocial:@"google" withParameters:params];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


// socialName - google for G+
//            - facebook for Facbook :)
// info - json with such fields: email, socialId, firstName, lastName, sex, looking, age, profession, bio
- (void)logInBySocial:(NSString *)socialName
       withParameters:(NSDictionary *)info {
    
    NSString *completeURL = [base_url stringByAppendingString:@"db/user"];
    NSLog(@"%@ ID", [info objectForKey:@"id"]);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager PUT:completeURL parameters:@{@"email": [info objectForKey:@"email"],
                                          @"socialId": [info objectForKey:@"id"],
                                          @"typeVerification": socialName
                                          }
         success:^(NSURLSessionTask *task, id responseObject) {
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:[responseData objectForKey:@"access_token"] forKey:@"access_token"];
             [defaults setObject:[responseData objectForKey:@"refresh_token"] forKey:@"refresh_token"];
             [defaults setObject:[responseData objectForKey:@"role"] forKey:@"role"];
             [defaults synchronize];
             
             NSLog(@"%@ ROLE", [defaults stringForKey:@"role"]);
             if (![[defaults stringForKey:@"role"] isEqualToString:@"nothing"]) {
                 [self performSegueWithIdentifier:@"social" sender:self];
             } else {
                 [self performSegueWithIdentifier:@"selectMode" sender:self];
             }

         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

- (void)googleButtonPressed:(id)sender {
//    [self logInWithGoogle];
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

- (void)facebookButtonPressed:(id)sender {
    NSLog(@"FACEBOOK BUTTON PRESSED");
}

- (void)performSwipe:(UISwipeGestureRecognizer *)recognizer {
    UISwipeGestureRecognizerDirection direction = [recognizer direction];
    if (direction == UISwipeGestureRecognizerDirectionLeft) {
        // Increment cursor
        self.cursor += 1;
        // If cursor is outside bounds of array, wrap around.
        // Chose not to use % to be more explicit.
        if (self.cursor >= [self.radioArray count]) {
            self.cursor = 0;
        }
        self.isForwardAnimation = YES;
    }
    else if (direction == UISwipeGestureRecognizerDirectionRight) {
        // Decrement cursor
        self.cursor -= 1;
        // If outside bounds of array, wrap around.
        if (self.cursor < 0) {
            self.cursor = [self.radioArray count] - 1;
        }
        self.isForwardAnimation = NO;
    }
    // After adjusting the cursor, we update the color.
    [self radioButtonPressed:self.radioArray[self.cursor]];
}

- (void)radioButtonPressed:(UIButton *)sender {
    for (int i = 0; i < self.radioArray.count; i++) {
        UIButton *radio = self.radioArray[i];
        if ([radio isEqual:sender]) {
            [radio setImage:[UIImage imageNamed:@"pagination_active"] forState:UIControlStateNormal];
            NSLog(@"BACK IMAGE: %@", self.backgroundArray[i]);
            
            self.bottombackgroundView.image = [UIImage imageNamed:self.backgroundArray[i]];
            self.backgroundImageView.image = [UIImage imageNamed:self.backgroundArray[i]];
            
            CATransition *transition = [CATransition animation];
            transition.duration = 0.5f;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            if (self.isForwardAnimation) {
                transition.subtype = kCATransitionFromRight;
            } else {
                transition.subtype = kCATransitionFromLeft;
            }
            
            [self.backgroundImageView.layer addAnimation:transition forKey:nil];

        } else {
            [radio setImage:[UIImage imageNamed:@"pagination"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - sharedInstance

+ (ViewController *)sharedInstance {
    static dispatch_once_t pred;
    static ViewController *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[ViewController alloc] init];
    });
    return sharedInstance;
}

@end
