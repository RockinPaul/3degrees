//
//  DateProposalsViewController.m
//  3Degrees
//
//  Created by Pavel on 28.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "DateProposalsViewController.h"

@interface DateProposalsViewController ()

@end

@implementation DateProposalsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textView.contentInset = UIEdgeInsetsMake(0, -5, 0, 0);
}

- (void)viewDidAppear:(BOOL)animated {
    
    // Autoresizing scroll view content size
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    
    contentRect.size.height = contentRect.size.height + 10; // More bottom spacing because of tab bar
    self.scrollView.contentSize = contentRect.size;
}

#pragma mark - Profile button pressed

- (void)profileButtonPressed:(id)sender {
    UIStoryboard *storyboard = self.storyboard;
    ProfileViewController *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profile"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:profileVC animated:NO completion:nil];
}

@end
