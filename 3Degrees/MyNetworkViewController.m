//
//  MyNetworkViewController.h
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "MyNetworkViewController.h"

@implementation MyNetworkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateNetwork)
                                                 name:@"networkReceived"
                                               object:nil];
    [self singlesButtonPressed:nil];

    self.networkArray = [NSArray new];
    self.singlesArray = [NSArray new];
    self.matchmakersArray = [NSArray new];
//    self.networkArray = @[@"Luisa Abel", @"Patricia DeHaney", @"Darryl Flynn", @"Josh Bernett", @"Kathryn Bigelow", @"Maggie Brattson", @"Casey Grant", @"Lauren Abiouness", @"Selena Bhargava", @"Sharon O'Neal"];
}

- (void)updateNetwork {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *role = [defaults stringForKey:@"role"];
    NSData *singles = nil;
    NSData *matchmakers = nil;
    if ([role isEqualToString:@"single"]) {
        singles = [[NSUserDefaults standardUserDefaults] objectForKey:@"singlesForSingleArray"];
        matchmakers = [[NSUserDefaults standardUserDefaults] objectForKey:@"matchmakersForSingleArray"];
        self.singlesArray = [NSKeyedUnarchiver unarchiveObjectWithData:singles];
        self.matchmakersArray = [NSKeyedUnarchiver unarchiveObjectWithData:matchmakers];
    } else if ([role isEqualToString:@"matchmaker"]) {
        singles = [[NSUserDefaults standardUserDefaults] objectForKey:@"singlesForMatchmakerArray"];
        matchmakers = [[NSUserDefaults standardUserDefaults] objectForKey:@"matchmakersForMatchmakerArray"];
        self.singlesArray = [NSKeyedUnarchiver unarchiveObjectWithData:singles];
        self.matchmakersArray = [NSKeyedUnarchiver unarchiveObjectWithData:matchmakers];
    } else {
        NSLog(@"ERROR: ROLE IS UNDEFINE");
    }
    
    if (self.isSingles) {
        self.networkArray = self.singlesArray;
    } else {
        self.networkArray = self.matchmakersArray;
    }
    [self.tableView reloadData];
}

#pragma mark - Cell for row at indexPath

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = nil;
    
    if (indexPath.row > 0) {
        cellIdentifier = @"Cell";
        MyNetworkCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[MyNetworkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
    
        Matchmaker *matchmaker = nil;
        if (self.isSingles) {
            matchmaker = self.singlesArray[indexPath.row];
            cell.mmCountLabel.text = @"";
        } else {
            matchmaker = self.matchmakersArray[indexPath.row];
            cell.mmCountLabel.text = [NSString stringWithFormat:@"%@ Singles", matchmaker.countSingle];
        }
        cell.usernameLabel.text = [NSString stringWithFormat:@"%@ %@", matchmaker.firstName, matchmaker.lastName];
        cell.avatarImageView.image = [UIImage imageNamed:@"blank_round"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            NSData *imageData = nil;
            imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:matchmaker.imageLink]];
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.avatarImageView.image = image;
                });
            }
        });
        return cell;
    } else {
        cellIdentifier = @"Cell_add";
        AddHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (self.isSingles) {
            [cell.titleButton setTitle:@"Add a New Single" forState:UIControlStateNormal];
        } else {
            [cell.titleButton setTitle:@"Add a New Matchmaker" forState:UIControlStateNormal];
        }
        return cell;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.networkArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isSingles) {
        Single *single = self.singlesArray[indexPath.row];
        [self singleInfoByID:single.iD];
    } else {
        Matchmaker *matchmaker = self.matchmakersArray[indexPath.row];
        [self matchmakerInfoByID:matchmaker.iD];
    }
}

#pragma mark - Matchmaker info by ID

- (void)matchmakerInfoByID:(NSString *)iD {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults stringForKey:@"access_token"];
    NSString *completeURL = [NSString stringWithFormat:@"%@search/user?access_token=%@&&matchmakersId=%@", base_url, accessToken, iD];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager GET:completeURL parameters:nil progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {

             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSMutableArray *array = [NSMutableArray new];
             Single *single = nil;
             
             for (NSDictionary *singleDict in responseData) {
                 single = [Single new];
                 single.firstName = [singleDict objectForKey:@"firstName"];
                 single.lastName = [singleDict objectForKey:@"lastName"];
                 single.email = [singleDict objectForKey:@"email"];
                 single.iD = [singleDict objectForKey:@"id"];
                 single.profession = [singleDict objectForKey:@"profession"];
                 single.job = [singleDict objectForKey:@"organization"];
                 single.imageLink = [singleDict objectForKey:@"imageCircle"];
                 single.sex = [singleDict objectForKey:@"sex"];
                 single.bio = [singleDict objectForKey:@"bio"];
                 single.location = [singleDict objectForKey:@"location"];
                 single.age = [singleDict objectForKey:@"age"];
                 single.userDescription = [singleDict objectForKey:@"description"];
                 NSDictionary *education = [singleDict objectForKey:@"education"];
                 single.institution = [education objectForKey:@"institution"];
                 NSDictionary *degree = [education objectForKey:@"degree"];
                 
                 if (![degree isEqual:(NSDictionary *)[NSNull null]]) {
                     single.degreeId = [degree objectForKey:@"id"];
                     single.degreeTitle = [degree objectForKey:@"title"];
                 }
                 [array addObject:single];
             }
             
             NSDictionary *matchmakerDict = [responseObject objectForKey:@"matchmaker"];
             NSLog(@"MATCHMAKER: %@", matchmakerDict);
             NSString *firstName = [matchmakerDict objectForKey:@"firstName"];
             NSString *lastName = [matchmakerDict objectForKey:@"lastName"];
             NSString *imageLink = [matchmakerDict objectForKey:@"image"];
             NSString *description = [matchmakerDict objectForKey:@"description"];
             MatchmakerDetailsViewController *mmDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MatchmakerDetails"];
             mmDetailsVC.userName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
             mmDetailsVC.userDescription = description;
             NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageLink]];
             UIImage *image = [[UIImage alloc] initWithData:imageData];
             if (image) {
                 mmDetailsVC.coverImage = image;
             }
             mmDetailsVC.singlesArray = array;
             [self.navigationController pushViewController:mmDetailsVC animated:YES];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

#pragma mark - Single info by ID

- (void)singleInfoByID:(NSString *)iD {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults stringForKey:@"access_token"];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/user/%@?access_token=%@", base_url, iD, accessToken];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager GET:completeURL parameters:nil progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSLog(@"%@ RESPONSE DATA", responseData);

             Single *single = [Single new];
             single.firstName = [responseData objectForKey:@"firstName"];
             single.lastName = [responseData objectForKey:@"lastName"];
             single.userDescription = [responseData objectForKey:@"description"];
             single.profession = [responseData objectForKey:@"profession"];
             single.job = [responseData objectForKey:@"organization"];
             single.bio = [responseData objectForKey:@"bio"];
             single.imageLink = [responseData objectForKey:@"imageNetwork"];
             NSDictionary *education = [responseData objectForKey:@"education"];
             single.institution = [education objectForKey:@"institution"];
             NSDictionary *degree = [education objectForKey:@"degree"];
             if (degree != (NSDictionary *)[NSNull null]) {
                 single.degreeTitle = [degree objectForKey:@"title"];
             }
             NSLog(@"%@", single.userDescription);
             
             SingleDetailsViewController *singleDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleDetails"];
             
             singleDetailsVC.userNameLabel.text = [NSString stringWithFormat:@"%@ %@", single.firstName, single.lastName];
             singleDetailsVC.userDescriptionLabel.text = single.userDescription;
             singleDetailsVC.positionLabel.text = single.profession;
             singleDetailsVC.jobLabel.text = single.job;
             singleDetailsVC.degreeLabel.text = single.degreeTitle;
             singleDetailsVC.institutionLabel.text = single.institution;
             singleDetailsVC.bioTextView.text = single.bio;
             NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:single.imageLink]];
             UIImage *image = [[UIImage alloc] initWithData:imageData];
             if (image) {
                 singleDetailsVC.coverImage = image;
             }
             [self.navigationController pushViewController:singleDetailsVC animated:YES];
             
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

#pragma mark - Add button pressed

- (void)addButtonPressed:(id)sender {
//    UIStoryboard *storyboard = self.storyboard;
//    UINavigationController *navController = [storyboard instantiateViewControllerWithIdentifier:@"invite"];
//    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.4;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self presentViewController:navController animated:NO completion:nil];
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        controller.mailComposeDelegate = self;
        [controller setSubject:@"3Degrees"];
        [controller setMessageBody:@"Invite text" isHTML:NO];
        controller.mailComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            [self showAlertWithMessage:@"Composed Mail is saved in draft."];
            break;
        case MFMailComposeResultSent:
            [self showAlertWithMessage:@"You have successfully referred your friends."];
            break;
        case MFMailComposeResultFailed:
            [self showAlertWithMessage:@"Sorry! Failed to send."];
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)showAlertWithMessage:(NSString *)message {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Attention"
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)singlesButtonPressed:(id)sender {
    self.isSingles = YES;
    [self.singlesButton setBackgroundImage:[UIImage imageNamed:@"button_network"] forState:UIControlStateNormal];
    [self.matchmakersButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.singlesButton setTitleColor:[ImageHandler colorWithHexString:@"B3ACA0"] forState:UIControlStateNormal];
    [self.matchmakersButton setTitleColor:[ImageHandler colorWithHexString:@"221F20"] forState:UIControlStateNormal];
    
    self.networkArray = self.singlesArray;
    [self.tableView reloadData];
}

- (void)matchmakersButtonPressed:(id)sender {
    self.isSingles = NO;
    [self.singlesButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.matchmakersButton setBackgroundImage:[UIImage imageNamed:@"button_network"] forState:UIControlStateNormal];
    [self.singlesButton setTitleColor:[ImageHandler colorWithHexString:@"221F20"] forState:UIControlStateNormal];
    [self.matchmakersButton setTitleColor:[ImageHandler colorWithHexString:@"B3ACA0"] forState:UIControlStateNormal];
    
    self.networkArray = self.matchmakersArray;
    [self.tableView reloadData];
}

@end
