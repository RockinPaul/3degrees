//
//  LocationHandler.m
//  3Degrees
//
//  Created by Pavel on 27.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "LocationHandler.h"

#define METERS_TO_FEET  3.2808399
#define METERS_TO_MILES 0.000621371192
#define METERS_CUTOFF   1000
#define FEET_CUTOFF     3281
#define FEET_IN_MILES   5280

@implementation LocationHandler

#pragma mark - User latitude

- (double)getUserLatitude {
    return self.locationManager.location.coordinate.latitude;
}

#pragma mark - User longitude

- (double)getUserLongitude {
    return self.locationManager.location.coordinate.longitude;
}

#pragma mark - Shared instance

+ (LocationHandler *)sharedInstance {
    static dispatch_once_t pred;
    static LocationHandler *sharedInstance = nil;
    dispatch_once(&pred, ^ {
        sharedInstance = [[LocationHandler alloc] init];
        sharedInstance.locationManager = [[CLLocationManager alloc] init];
        sharedInstance.locationManager.delegate = sharedInstance;
        sharedInstance.locationManager.distanceFilter = kCLDistanceFilterNone;
        sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [sharedInstance.locationManager requestWhenInUseAuthorization];
        }
        [sharedInstance.locationManager startUpdatingLocation];
    });
    return sharedInstance;
}

@end
