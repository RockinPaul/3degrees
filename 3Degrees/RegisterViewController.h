//
//  RegisterViewController.h
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignInViewController.h"

@interface RegisterViewController : SignInViewController

@end
