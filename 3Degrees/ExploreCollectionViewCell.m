//
//  ExploreCollectionViewCell.m
//  3Degrees
//
//  Created by Pavel on 12.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ExploreCollectionViewCell.h"

@implementation ExploreCollectionViewCell

- (void)awakeFromNib {
    [self addSubview:self.usernameLabel];
    [self addSubview:self.locationLabel];
    [self addSubview:self.imageView];
}

@end
