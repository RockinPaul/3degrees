//
//  ExploreCollectionViewCell.h
//  3Degrees
//
//  Created by Pavel on 12.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UILabel *usernameLabel;
@property (nonatomic, strong) IBOutlet UILabel *locationLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end
