//
//  ExploreContainerViewController.h
//  3Degrees
//
//  Created by Pavel on 14.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>
#import "RESThelper.h"
#import "FilterViewController.h"

@interface ExploreContainerViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

- (IBAction)filterButtonPressed:(UIButton *)sender;

@end
