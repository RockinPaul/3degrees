//
//  ViewController.h
//  3Degrees
//
//  Created by Pavel on 07.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>
#import "Constants.h"
#import "RESThelper.h"
#import "TabBarController.h"
#import <Google/SignIn.h>

@interface ViewController : UIViewController <UIGestureRecognizerDelegate, GIDSignInUIDelegate>

@property (nonatomic, strong) IBOutlet UIButton *radio1;
@property (nonatomic, strong) IBOutlet UIButton *radio2;
@property (nonatomic, strong) IBOutlet UIButton *radio3;
@property (nonatomic, strong) IBOutlet UIButton *radio4;
@property (nonatomic, strong) IBOutlet UIButton *radio5;
@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic) long cursor;
@property (nonatomic, strong) NSArray *radioArray;
@property (nonatomic, strong) NSArray *backgroundArray;
@property (nonatomic) BOOL isForwardAnimation;
@property (nonatomic) BOOL isAlreadyLogged;
@property (nonatomic, strong) UIImageView *bottombackgroundView;

- (IBAction)facebookButtonPressed:(id)sender;
- (IBAction)googleButtonPressed:(id)sender;

- (void)logInBySocial:(NSString *)socialName
       withParameters:(NSDictionary *)info;

- (IBAction)performSwipe:(UISwipeGestureRecognizer *)recognizer;
- (IBAction)radioButtonPressed:(UIButton *)sender;

+ (ViewController *)sharedInstance;

@end

