//
//  SignInViewController.h
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "ViewController.h"

@interface SignInViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet CustomTextField *emailTextField;
@property (nonatomic, strong) IBOutlet CustomTextField *passwordTextField;
@property (nonatomic, strong) IBOutlet UIButton *nextButton;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)nextButtonPressed:(id)sender;

@end
