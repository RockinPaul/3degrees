//
//  FilterView.h
//  3Degrees
//
//  Created by Pavel on 22.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ImageHandler.h"

@interface FilterView : UIView

@property (nonatomic, strong) IBOutlet UILabel *textLabel;

- (void)setViewActive;
- (void)setViewInactive;
- (void)setViewText:(NSString *)text;

@end
