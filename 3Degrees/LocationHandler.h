//
//  LocationHandler.h
//  3Degrees
//
//  Created by Pavel on 27.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface LocationHandler : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;

- (double)getUserLatitude;
- (double)getUserLongitude;
+ (LocationHandler *)sharedInstance;

@end
