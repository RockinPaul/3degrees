//
//  RESThelper.m
//  3Degrees
//
//  Created by Pavel on 05.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "RESThelper.h"

@implementation RESThelper

#pragma mark - Error

+ (NSString *)errorDescription:(NSError *)error {
    // Getting error description
    NSData *data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSString *failureResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSData *errorData = [failureResponse dataUsingEncoding:NSUTF8StringEncoding];
    id failureJSON = [NSJSONSerialization JSONObjectWithData:errorData options:0 error:nil];
    NSLog(@"API error: %@", [failureJSON objectForKey:@"error"]);
    return [failureJSON objectForKey:@"error"];
}

#pragma mark - Password with Salt

+ (NSString *)passwordWithSaltFromPassword:(NSString *)password {
    NSMutableString *passwordWithSalt = [NSMutableString stringWithFormat:@"%@%@%@", salt, password, salt];
    NSString *passwordString = [SHA1Encoder stringToSha1:passwordWithSalt];
    NSString *stringFromUTFString = [[NSString alloc] initWithUTF8String:[passwordString UTF8String]];
    return stringFromUTFString;
}

#pragma mark - Perform selector

+ (void)performRequest:(SEL)selector
             ForTarget:(id)target
        WithParameters:(NSDictionary *)params {
    NSString *completeURL = [base_url stringByAppendingString:@"db/token"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *refreshToken = [defaults stringForKey:@"refresh_token"];
    NSLog(@"%@", refreshToken);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager POST:completeURL parameters:@{@"refresh_token": refreshToken}
         progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSLog(@"%@ TOKENS", responseData);
             NSString *accessToken = [responseData objectForKey:@"access_token"];
             NSString *refreshToken = [responseData objectForKey:@"refresh_token"];
             [defaults setObject:accessToken forKey:@"access_token"];
             [defaults setObject:refreshToken forKey:@"refresh_token"];
             
             // Perform selector here
//             if (!self) { return; }
//             IMP imp = [self methodForSelector:selector];
//             void (*func)(id, SEL, id) = (void *)imp;
//             func(target, selector, params);
             if (selector != nil) {
                 [target performSelector:selector withObject:params];
             }
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"tokens_updated"
                                                                 object:self];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

@end
