//
//  Activity.h
//  3Degrees
//
//  Created by Pavel on 14.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Activity : NSObject

@property (nonatomic, strong) NSString *matchmaker;
@property (nonatomic, strong) NSString *singleName;
@property (nonatomic, strong) NSString *singleDescription;
@property (nonatomic, strong) NSString *singleAvatar;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *type; // invited for single
                                              // watching/accepted for matchmaker
// FOR ACCEPTED TYPE
@property (nonatomic, strong) NSString *singleOneAvatar;
@property (nonatomic, strong) NSString *singleTwoAvatar;
@property (nonatomic, strong) NSString *singleOneDescription;
@property (nonatomic, strong) NSString *singleTwoDescription;
@property (nonatomic, strong) NSString *singleOneName;
@property (nonatomic, strong) NSString *singleTwoName;

@end
