//
//  TabBarController.h
//  3Degrees
//
//  Created by Pavel on 22.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESThelper.h"
#import "LocationHandler.h"
#import "Constants.h"
#import "Single.h"
#import "Matchmaker.h"

@interface TabBarController : UITabBarController <UITabBarControllerDelegate>

@property (nonatomic, strong) NSArray *singlesForMatchmakerArray;
@property (nonatomic, strong) NSArray *singlesForSingleArray;
@property (nonatomic, strong) NSArray *matchmakersForMatchmakerArray;
@property (nonatomic, strong) NSArray *matchmakersForSingleArray;

@end
