//
//  ChooseLocationsViewController.m
//  3Degrees
//
//  Created by Pavel on 22.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ChooseLocationsViewController.h"

#define BOTTOM 44.0

@implementation ChooseLocationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@", self.locationsArray);
    self.selectedLocationsIDs = [NSMutableArray new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"Cell";
    LocationCell *cell = (LocationCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.locationLabel.text = [self.locationsArray[indexPath.row] objectForKey:@"title"];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%lu COUNT", (unsigned long)self.locationsArray.count);
    return self.locationsArray.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.selectedLocationsIDs addObject:[self.locationsArray[indexPath.row] objectForKey:@"id"]];
    NSLog(@"%@ SELECT", self.selectedLocationsIDs);
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.selectedLocationsIDs removeObject:[self.locationsArray[indexPath.row] objectForKey:@"id"]];
    NSLog(@"%@ DESELECt", self.selectedLocationsIDs);
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)openButtonPressed:(UIButton *)sender {
//    NSLog(@"%f HEIGHT", self.tableViewHeight);
//    if (!self.isOpen) {
//        self.heightConstraint.constant -= self.tableViewHeight;
//        [UIView animateWithDuration:0.75 animations:^{
//            [self.view layoutIfNeeded];
//        } completion:^(BOOL finished) {
//            NSLog(@"Open");
//        }];
//        
//    } else {
//        self.heightConstraint.constant += self.tableViewHeight;
//        [UIView animateWithDuration:0.75 animations:^{
//        } completion:^(BOOL finished) {
//            [self.view layoutIfNeeded];
//            NSLog(@"Close");
//        }];
//        
//    }
//    
//    self.isOpen =!self.isOpen;

}

@end
