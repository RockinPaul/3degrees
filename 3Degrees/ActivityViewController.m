//
//  ActivityViewController.m
//  3Degrees
//
//  Created by Pavel on 28.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "ActivityViewController.h"

@interface ActivityViewController ()

@end

@implementation ActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.contentInset = UIEdgeInsetsMake(15, 0, 0, 0);
    self.activityArray = [NSArray new];
    
    [self updateActivity];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateActivity)
                                                 name:@"pair_up"
                                               object:nil];
}

- (void)updateActivity {
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjects:@[@"0", @"1000"] forKeys:@[@"offset", @"limit"]];
    SEL activitySelector = @selector(activityWithParameters:);
    [RESThelper performRequest:activitySelector ForTarget:self WithParameters:parameters];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = nil;
    Activity *activity = self.activityArray[indexPath.row];
    NSLog(@"%@", activity.text);
    
    if ([activity.type isEqualToString:@"accepted"]) {
        cellIdentifier = @"Accepted";
        AcceptedCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.text.text = activity.text;
        cell.singleOneName.text = activity.singleOneName;
        cell.singleTwoName.text = activity.singleTwoName;
        cell.singleOneDescription.text = activity.singleOneDescription;
        cell.singleTwoDescription.text = activity.singleTwoDescription;
        cell.singleOneImageView.image = [UIImage imageNamed:@"blank_round"];
        cell.singleTwoImageView.image = [UIImage imageNamed:@"blank_round"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            NSData *firstData = [NSData dataWithContentsOfURL:[NSURL URLWithString:activity.singleOneAvatar]];
            NSData *secondData = [NSData dataWithContentsOfURL:[NSURL URLWithString:activity.singleTwoAvatar]];
            UIImage *firstAvatar = [[UIImage alloc] initWithData:firstData];
            UIImage *secondAvatar = [[UIImage alloc] initWithData:secondData];
            if (firstAvatar) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.singleOneImageView.image = firstAvatar;
                });
            }
            if (secondAvatar) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.singleTwoImageView.image = secondAvatar;
                });
            }
        });
        return cell;
        
    } else if ([activity.type isEqualToString:@"watching"] || ([activity.type isEqualToString:@"invited"])) {
        cellIdentifier = @"Proposed";
        ProposedCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.singleName.text = activity.singleName;
        cell.singleDescription.text = activity.singleDescription;
        cell.text.text = activity.text;
        cell.matchmakerLabel.text = activity.matchmaker;
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:activity.singleAvatar]];
            UIImage *image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.singleImageView.image = image;
                });
            }
        });
        return cell;
    } else {
        NSLog(@"Error: Something wrong with cell type");
        return [tableView dequeueReusableCellWithIdentifier:@"Proposed"];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.activityArray.count;
}

#pragma mark - Activity

- (void)activityWithParameters:(NSDictionary *)params {
    NSString *offset = [params objectForKey:@"offset"];
    NSString *limit = [params objectForKey:@"limit"];
    [self activityWithOffset:offset Limit:limit];
}

- (void)activityWithOffset:(NSString *)offset
                     Limit:(NSString *)limit {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults stringForKey:@"access_token"];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/activity?access_token=%@&&limit=%@&&offset=%@", base_url, accessToken, limit, offset];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager GET:completeURL
      parameters:nil
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             Activity *activity = nil;
             NSMutableArray *activities = [NSMutableArray new];
             for (NSDictionary *activityDict in responseData) {
                 activity = [Activity new];
                 activity.text = [activityDict objectForKey:@"text"];
                 activity.type = [activityDict objectForKey:@"type"];
                 if ([activity.type isEqualToString:@"accepted"]) {
                     NSDictionary *singleOne = [activityDict objectForKey:@"singleOne"];
                     NSDictionary *singleTwo = [activityDict objectForKey:@"singleTwo"];
                     activity.singleOneAvatar = [singleOne objectForKey:@"avatar"];
                     activity.singleOneDescription = [singleOne objectForKey:@"description"];
                     activity.singleOneName = [singleOne objectForKey:@"name"];
                     activity.singleTwoAvatar = [singleTwo objectForKey:@"avatar"];
                     activity.singleTwoDescription = [singleTwo objectForKey:@"description"];
                     activity.singleTwoName = [singleTwo objectForKey:@"name"];
                 } else if ([activity.type isEqualToString:@"watching"] || [activity.type isEqualToString:@"invited"]) {
                     activity.matchmaker = [activityDict objectForKey:@"matchmaker"];
                     NSDictionary *single = [activityDict objectForKey:@"single"];
                     activity.singleAvatar = [single objectForKey:@"avatar"];
                     activity.singleDescription = [single objectForKey:@"description"];
                     activity.singleName = [single objectForKey:@"name"];
                 }
                 [activities addObject:activity];
                 NSLog(@"TEXT: %@", activity.text);
             }
             self.activityArray = activities;
             
             [self.tableView reloadData];
             NSLog(@"%@ ACTIVITY", responseData);
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

#pragma mark - Profile button pressed

- (void)profileButtonPressed:(id)sender {
    UIStoryboard *storyboard = self.storyboard;
    ProfileViewController *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profile"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:profileVC animated:NO completion:nil];
}


@end
