//
//  ExploreContainerViewController.m
//  3Degrees
//
//  Created by Pavel on 14.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ExploreContainerViewController.h"

@implementation ExploreContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    
    // Autoresizing scroll view content size
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    contentRect.size.height = contentRect.size.height + 10; // More bottom spacing because of tab bar
    self.scrollView.contentSize = contentRect.size;
}

- (void)filterButtonPressed:(UIButton *)sender {
    UIViewController *filterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    filterVC.view.tag = 1;
    [self.view addSubview:filterVC.view];

    filterVC.view.frame = CGRectMake(filterVC.view.frame.origin.x,
                                                -filterVC.view.frame.size.height,
                                                filterVC.view.frame.size.width,
                                                filterVC.view.frame.size.height);
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void){
        
        self.tabBarController.tabBar.hidden = YES;
        filterVC.view.frame = CGRectMake(filterVC.view.frame.origin.x,
                                                    0,
                                         filterVC.view.frame.size.width,
                                         filterVC.view.frame.size.height);
        
    } completion:^(BOOL finished){
        
        UIViewController *filterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
        [self presentViewController:filterVC animated:NO completion:^{
            self.tabBarController.tabBar.hidden = NO;
            for (UIView *subView in self.view.subviews)
            {
                if (subView.tag == 1)
                {
                    [subView removeFromSuperview];
                }
            }
        }];
    }];
}

@end
