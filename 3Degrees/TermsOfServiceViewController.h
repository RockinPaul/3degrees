//
//  TermsOfServiceViewController.h
//  3Degrees
//
//  Created by Pavel on 25.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsOfServiceViewController : UIViewController

- (IBAction)backButtonPressed:(id)sender;

@end
