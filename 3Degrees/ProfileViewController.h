//
//  ProfileViewController.h
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuItemCell.h"
#import "ImageHandler.h"
#import <AFNetworking.h>
#import "Constants.h"
#import "RESThelper.h"

@interface ProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) NSArray *generalItems;
@property (nonatomic, strong) NSArray *supportItems;
@property (nonatomic, strong) NSArray *aboutItems;

@property (nonatomic, strong) IBOutlet UIButton *generalButton;
@property (nonatomic, strong) IBOutlet UIButton *supportButton;
@property (nonatomic, strong) IBOutlet UIButton *aboutButton;

@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UIImageView *bluredImageView;
@property (nonatomic, strong) IBOutlet UILabel *usernameLabel;
@property (nonatomic, strong) IBOutlet UILabel *ageAndLocationLabel;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)generalButtonPressed:(id)sender;
- (IBAction)supportButtonPressed:(id)sender;
- (IBAction)aboutButtonPressed:(id)sender;


@end
