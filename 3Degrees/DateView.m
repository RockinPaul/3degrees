//
//  DateView.m
//  3Degrees
//
//  Created by Pavel on 03.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "DateView.h"

@implementation DateView

- (void)drawRect:(CGRect)rect {
    self.layer.borderWidth = 1;
    self.layer.borderColor = [[ImageHandler colorWithHexString:@"E6E6E6"] CGColor];
    
}

@end
