//
//  SchedulingViewController.m
//  3Degrees
//
//  Created by Pavel on 26.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "SchedulingViewController.h"

@interface SchedulingViewController ()

@end

@implementation SchedulingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dateView.userInteractionEnabled = YES;
    self.timeView.userInteractionEnabled = YES;
    self.locationView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *dateGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateViewPressed:)];
    [self.dateView addGestureRecognizer:dateGesture];
    
    UITapGestureRecognizer *timeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(timeViewPressed:)];
    [self.timeView addGestureRecognizer:timeGesture];
    
    UITapGestureRecognizer *locationGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationViewPressed:)];
    [self.locationView addGestureRecognizer:locationGesture];
}

- (void)dateViewPressed:(UITapGestureRecognizer *)sender {
    ChooseDatesViewController *chooseDatesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"chooseDates"];
    [self.navigationController pushViewController:chooseDatesVC animated:YES];
}

- (void)timeViewPressed:(UITapGestureRecognizer *)sender {
    ChooseDatesViewController *chooseTimesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"chooseTimes"];
    [self.navigationController pushViewController:chooseTimesVC animated:YES];
}

- (void)locationViewPressed:(UITapGestureRecognizer *)sender {    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjects:@[@"100", @"0"] forKeys:@[@"limit", @"offset"]];
    SEL selector = @selector(locationsForParameters:);
    [RESThelper performRequest:selector ForTarget:self WithParameters:parameters];
}

#pragma mark - Locations

// Wrapper for selector
- (void)locationsForParameters:(NSDictionary *)params {
    NSString *limit = [params objectForKey:@"limit"];
    NSString *offset = [params objectForKey:@"offset"];
    [self locationsListWithLimit:limit Offset:offset];
}

- (void)locationsListWithLimit:(NSString *)limit
                        Offset:(NSString *)offset {
    NSLog(@"%@ LIMIT", limit);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/locations?access_token=%@&&limit=%@&&offset=%@", base_url, [defaults stringForKey:@"access_token"],  limit, offset];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager GET:completeURL parameters:nil progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSMutableArray *locationsDictArray = [NSMutableArray new];
             for (NSDictionary *location in responseData) {
                 [locationsDictArray addObject:location];
             }
             self.locationsArray = (NSArray *)locationsDictArray;
             
             ChooseLocationsViewController *chooseLocationsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"chooseLocations"];
             chooseLocationsVC.locationsArray = self.locationsArray;
             [self.navigationController pushViewController:chooseLocationsVC animated:YES];
             
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

@end
