//
//  RegisterViewController.m
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "RegisterViewController.h"

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)nextButtonPressed:(id)sender {
    NSString *email = self.emailTextField.text;
    NSString *password = [RESThelper passwordWithSaltFromPassword:self.passwordTextField.text];
    NSLog(@"PASSWORD: %@", password);
    NSString *completeURL = [base_url stringByAppendingString:@"db/user"];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager POST:completeURL parameters:@{@"email": email,
                                           @"password": password
                                           }
         progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:[[responseObject objectForKey:@"data"] objectForKey:@"access_token"] forKey:@"access_token"];
             [defaults setObject:[[responseObject objectForKey:@"data"] objectForKey:@"refresh_token"] forKey:@"refresh_token"];
             [defaults synchronize];
             [self performSegueWithIdentifier:@"sign_up" sender:self]; // go to mode screen

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        // Alert
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:[RESThelper errorDescription:error] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }];
}

@end
