//
//  FilterViewController.h
//  3Degrees
//
//  Created by Pavel on 21.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterView.h"
#import "Constants.h"
#import <AFNetworking.h>
#import "RESThelper.h"
#import "Single.h"
#import "FilterController.h"

FOUNDATION_EXPORT float const width;
FOUNDATION_EXPORT float const height;
FOUNDATION_EXPORT float const leftPickerMargin;
FOUNDATION_EXPORT float const rightPickerMargin;
FOUNDATION_EXPORT float const topMargin;

@interface FilterViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSMutableSet *views;
@property (nonatomic, strong) IBOutlet FilterView *ageView;
@property (nonatomic, strong) IBOutlet FilterView *sexView;
@property (nonatomic, strong) IBOutlet FilterView *distanceView;
@property (nonatomic, strong) IBOutlet FilterView *matchmakerView;
@property (nonatomic, strong) FilterView *currentView;

@property (nonatomic, strong) NSArray *ageArray;
@property (nonatomic, strong) NSArray *sexArray;
@property (nonatomic, strong) NSArray *distanceArray;
@property (nonatomic, strong) NSArray *matchmakersArray;
@property (nonatomic, strong) NSArray *matchmakersIdsArray;

@property (nonatomic, strong) UIPickerView *ageFromPicker;
@property (nonatomic, strong) UIPickerView *ageToPicker;
@property (nonatomic, strong) UIPickerView *sexPicker;
@property (nonatomic, strong) UIPickerView *distancePicker;
@property (nonatomic, strong) UIPickerView *matchmakerPicker;
@property (nonatomic, strong) UIPickerView *currentPicker;
@property (nonatomic, strong) NSArray *pickers;
@property (nonatomic, strong) NSArray *currentArray;

@property (nonatomic, strong) NSString *ageMax;
@property (nonatomic, strong) NSString *ageMin;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *matchmakersId;

@property (nonatomic, strong) UIView *titleView;

- (IBAction)filterButtonPressed:(UIButton *)sender;
- (void)filterViewPressed:(UIView *)sender;

@end
