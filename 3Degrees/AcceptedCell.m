//
//  AcceptedCell.m
//  3Degrees
//
//  Created by Pavel on 10.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "AcceptedCell.h"

@implementation AcceptedCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
