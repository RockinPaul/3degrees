//
//  ChooseTimesViewController.m
//  3Degrees
//
//  Created by Pavel on 22.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ChooseTimesViewController.h"

@implementation ChooseTimesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedTimesArray = [NSMutableArray new];
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)timeButtonPressed:(UIButton *)sender {
    NSString *time = sender.titleLabel.text;
    
    if ([self.selectedTimesArray containsObject:time]) {
        [self.selectedTimesArray removeObject:time];
        [sender setBackgroundImage:[UIImage imageNamed:@"choose_time_deselected"] forState:UIControlStateNormal];
        [sender setTitleColor:[ImageHandler colorWithHexString:@"221F20"] forState:UIControlStateNormal];
        NSLog(@"CONTAINS");
    } else {
        if (self.selectedTimesArray.count < 5) {
            [self.selectedTimesArray addObject:time];
            [sender setBackgroundImage:[UIImage imageNamed:@"choose_time_selected"] forState:UIControlStateNormal];
            [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            NSLog(@"DESELECTED");
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"You should to suggest up to 5 times" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:nil];
            [alertController addAction:okButton];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

@end
