//
//  Single.h
//  3Degrees
//
//  Created by Pavel on 10.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Single : NSObject <NSCoding>

@property (nonatomic, strong) NSString *iD;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *institution;
@property (nonatomic, strong) NSString *degreeId;
@property (nonatomic, strong) NSString *degreeTitle;
@property (nonatomic, strong) NSString *imageLink;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *looking;
@property (nonatomic, strong) NSString *profession;
@property (nonatomic, strong) NSString *job;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *age;
@property (nonatomic, strong) NSString *bio;
@property (nonatomic, strong) NSString *userDescription;

@end
