//
//  DateContainerViewController.h
//  3Degrees
//
//  Created by Pavel on 01.03.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDTSimpleCalendarViewController.h"
#import "PDTSimpleCalendarViewCell.h"

@interface DateContainerViewController : PDTSimpleCalendarViewController

@end
