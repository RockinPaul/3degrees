//
//  SignInViewController.m
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "SignInViewController.h"

@implementation SignInViewController

#define kOFFSET_FOR_KEYBOARD 80.0

- (void)viewDidLoad {
    [super viewDidLoad];
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    self.nextButton.alpha = 0;
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)nextButtonPressed:(id)sender {
#warning should to be solved
    [[NSURLCache sharedURLCache] removeAllCachedResponses]; // Remove all cached responses :) TODO: NEED TO DEACTIVATE CACHE ONLY FOR SPECIFIC REQUESTS!

    NSString *email = self.emailTextField.text;
    NSString *password = [RESThelper passwordWithSaltFromPassword:self.passwordTextField.text];
    NSString *completeURL = [base_url stringByAppendingString:@"db/user"];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager PUT:completeURL parameters:@{@"email": email,
                                          @"password": password,
                                          @"typeVerification": @"email"}
          success:^(NSURLSessionTask *task, id responseObject) {
              NSDictionary *responseData = [responseObject objectForKey:@"data"];
              NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
              [defaults setObject:[responseData objectForKey:@"access_token"] forKey:@"access_token"];
              [defaults setObject:[responseData objectForKey:@"refresh_token"] forKey:@"refresh_token"];
              [defaults setObject:[responseData objectForKey:@"role"] forKey:@"role"];
              [defaults synchronize];
              
              if (![[defaults stringForKey:@"role"] isEqualToString:@"nothing"]) {
                  TabBarController *tabBarController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarController"];
                  [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:tabBarController animated:YES completion:^{
                      [self textFieldShouldReturn:self.emailTextField];
                  }];
              } else {
                  [self performSegueWithIdentifier:@"fromSignInToMode" sender:self];
              }
              
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
             
             // Alert
             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"invalid credentials" preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
             [alertController addAction:ok];
             [self presentViewController:alertController animated:YES completion:nil];
    }];
}


#pragma mark - UITextField delegate

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    if ([sender isEqual:self.emailTextField] || ([sender isEqual:self.passwordTextField])) {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([self.emailTextField hasText] && [self.passwordTextField hasText]) {
        self.nextButton.alpha = 1.0;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (![textField hasText]) {
        self.nextButton.alpha = 0.0;
    }
    [self setViewMovedUp:NO];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self setViewMovedUp:NO];
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Keyboard management

//method to move the view up/down whenever the keyboard is shown/dismissed
- (void)setViewMovedUp:(BOOL)movedUp {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
//        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
//        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)dismissKeyboard {
    if ([self.emailTextField isFirstResponder] || [self.passwordTextField isFirstResponder]) {
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
    }
}

- (void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:NO];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:YES];
    }
}

- (void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
