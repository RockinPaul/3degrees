//
//  ContactCell.m
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

- (void)awakeFromNib {
    [self addSubview:self.usernameLabel];
    [self addSubview:self.phoneLabel];
    [self addSubview:self.sendButton];
}

@end
