//
//  Constants.h
//  3Degrees
//
//  Created by Pavel on 30.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#ifdef DEBUG
    #define salt @"d18f9cfb8" // specific salt of project backend
    #define base_url @"http://52.33.45.104/api/"
#else
    #define salt @"d18f9cfb8" // specific salt of project backend
    #define base_url @"http://52.33.45.104/api/"
#endif

@end
