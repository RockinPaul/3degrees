//
//  SingleDetailsViewController.h
//  3Degrees
//
//  Created by Pavel on 18.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleDetailsViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *userDescriptionLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *positionLabel;
@property (nonatomic, strong) IBOutlet UILabel *jobLabel;
@property (nonatomic, strong) IBOutlet UILabel *degreeLabel;
@property (nonatomic, strong) IBOutlet UILabel *institutionLabel;
@property (nonatomic, strong) IBOutlet UITextView *bioTextView;
@property (nonatomic, strong) UIImage *coverImage;

- (IBAction)backButtonPressed:(UIButton *)sender;

@end
