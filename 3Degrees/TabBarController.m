//
//  TabBarController.m
//  3Degrees
//
//  Created by Pavel on 22.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "TabBarController.h"

#define limit @"10000"
#define offset @"0"

@interface TabBarController() <UITabBarControllerDelegate>

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setDelegate:self];
        
    [[self.tabBar items] objectAtIndex:0].selectedImage = [[UIImage imageNamed:@"icon_tab_network_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    ;
    [[self.tabBar items] objectAtIndex:1].selectedImage = [[UIImage imageNamed:@"icon_tab_explore_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    ;
    [[self.tabBar items] objectAtIndex:2].selectedImage = [[UIImage imageNamed:@"icon_tab_activity_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    ;
    
    SEL coordinatesSelector = @selector(setUserCoordinates);
    [RESThelper performRequest:coordinatesSelector ForTarget:self WithParameters:nil];
}


- (void)setUserCoordinates {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults stringForKey:@"access_token"];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/user/gps?access_token=%@", base_url, accessToken];
    double latitude = [[LocationHandler sharedInstance] getUserLatitude];
    double longitude = [[LocationHandler sharedInstance] getUserLongitude];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager PUT:completeURL parameters:@{
                                          @"lat": [NSString stringWithFormat:@"%f", latitude],
                                          @"lon": [NSString stringWithFormat:@"%f", longitude]
                                          }
          success:^(NSURLSessionTask *task, id responseObject) {
              [self profileByToken];
          } failure:^(NSURLSessionTask *operation, NSError *error) {
              [RESThelper errorDescription:error];
          }];
}

#pragma mark - Look at this shit! Key method! Profile by token

- (void)profileByToken {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults stringForKey:@"access_token"];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/user?access_token=%@", base_url, accessToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager GET:completeURL
      parameters:nil
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSString *firstName = [responseData objectForKey:@"firstName"];
             NSString *lastName = [responseData objectForKey:@"lastName"];
             NSString *userName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
             NSString *age = [responseData objectForKey:@"age"];
             NSString *location = [responseData objectForKey:@"location"];
             NSString *imageLink = [responseData objectForKey:@"image"];
             NSString *bluredLink = [responseData objectForKey:@"blurImage"];
             
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:userName forKey:@"userName"];
             [defaults setObject:age forKey:@"age"];
             [defaults setObject:location forKey:@"location"];
             

             NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageLink]];
             NSData *bluredData = [NSData dataWithContentsOfURL:[NSURL URLWithString:bluredLink]];
             UIImage *image = [[UIImage alloc] initWithData:imageData];
             UIImage *blured = [[UIImage alloc] initWithData:bluredData];
             if (image) {
                     [defaults setObject:UIImagePNGRepresentation(image) forKey:@"image"];
             }
             if (blured) {
                     [defaults setObject:UIImagePNGRepresentation(blured) forKey:@"bluredImage"];
             }
             [defaults synchronize];
             
             if ([[defaults stringForKey:@"role"] isEqualToString:@"matchmaker"]) {
                 [self singlesForMatchmaker];
             } else if ([[defaults stringForKey:@"role"] isEqualToString:@"single"]) {
                 [self singlesForSingle];
             } else {
                 NSLog(@"ERROR: ROLE IS UNDEFINE");
             }
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}

#pragma mark - Singles for matchmaker
// 1
- (void)singlesForMatchmaker {
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^(void) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *accessToken = [defaults stringForKey:@"access_token"];
        NSString *completeURL = [NSString stringWithFormat:@"%@db/matchmaker/users?access_token=%@&&limit=%@&&offset=%@", base_url, accessToken, limit, offset];
        NSLog(@"%@ COMPLETE URL", completeURL);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = nil;
        [manager GET:completeURL
          parameters:nil
            progress:nil
             success:^(NSURLSessionTask *task, id responseObject) {
//                 dispatch_async(dispatch_get_main_queue(), ^{
                  NSMutableArray *singlesArray = [NSMutableArray new];
                     Single *single = nil;
                     NSDictionary *responseData = [responseObject objectForKey:@"data"];
                     for (NSDictionary *singleDict in responseData) {
                         single = [Single new];
                         single.email = [singleDict objectForKey:@"email"];
                         single.firstName = [singleDict objectForKey:@"firstName"];
                         single.lastName = [singleDict objectForKey:@"lastName"];
                         single.iD = [singleDict objectForKey:@"id"];
                         single.imageLink = [singleDict objectForKey:@"image"];
                         NSLog(@"%@ singlesForMatchmaker", single.imageLink);
                         [singlesArray addObject:single];
                     }
                     self.singlesForMatchmakerArray = singlesArray;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     NSData *dataFromArray = [NSKeyedArchiver archivedDataWithRootObject:self.singlesForMatchmakerArray];
                     [defaults setObject:dataFromArray forKey:@"singlesForMatchmakerArray"];
                     [defaults synchronize];
                     
                     [self matchmakersForMatchmaker];
//                 });
             } failure:^(NSURLSessionTask *operation, NSError *error) {
                 [RESThelper errorDescription:error];
             }];
//    });
}

#pragma mark - Singles for single
//1
- (void)singlesForSingle {
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^(void) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *accessToken = [defaults stringForKey:@"access_token"];
        NSString *completeURL = [NSString stringWithFormat:@"%@db/single/singles?access_token=%@&&limit=%@&&offset=%@", base_url, accessToken, limit, offset];
        NSLog(@"%@ COMPLETE URL", completeURL);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = nil;
        [manager GET:completeURL
          parameters:nil
            progress:nil
             success:^(NSURLSessionTask *task, id responseObject) {
//                 dispatch_async(dispatch_get_main_queue(), ^{
                   NSMutableArray *singlesArray = [NSMutableArray new];
                     Single *single = nil;
                     NSDictionary *responseData = [responseObject objectForKey:@"data"];
                     for (NSDictionary *singleDict in responseData) {
                         single = [Single new];
                         single.email = [singleDict objectForKey:@"email"];
                         single.firstName = [singleDict objectForKey:@"firstName"];
                         single.lastName = [singleDict objectForKey:@"lastName"];
                         single.iD = [singleDict objectForKey:@"id"];
                         single.imageLink = [singleDict objectForKey:@"image"];
                         NSLog(@"%@ singlesForSingle", singleDict);
                         [singlesArray addObject:single];
                     }
                     self.singlesForSingleArray = singlesArray;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     NSData *dataFromArray = [NSKeyedArchiver archivedDataWithRootObject:self.singlesForSingleArray];
                     [defaults setObject:dataFromArray forKey:@"singlesForSingleArray"];
                     [defaults synchronize];
                     
                     [self matchmakersForSingle];
//                 });
             } failure:^(NSURLSessionTask *operation, NSError *error) {
                 [RESThelper errorDescription:error];
             }];
//    });
}

#pragma mark - Matchmakers for matchmaker
//2
- (void)matchmakersForMatchmaker {
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^(void) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *accessToken = [defaults stringForKey:@"access_token"];
        NSString *completeURL = [NSString stringWithFormat:@"%@db/matchmakers?access_token=%@&&limit=%@&&offset=%@", base_url, accessToken, limit, offset];
        NSLog(@"%@ COMPLETE URL", completeURL);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = nil;
        [manager GET:completeURL
          parameters:nil
            progress:nil
             success:^(NSURLSessionTask *task, id responseObject) {
//                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSMutableArray *matchmakersArray = [NSMutableArray new];
                     Matchmaker *matchmaker = nil;
                     NSDictionary *responseData = [responseObject objectForKey:@"data"];
                     for (NSDictionary *mmDict in responseData) {
                         matchmaker = [Matchmaker new];
                         matchmaker.email = [mmDict objectForKey:@"email"];
                         matchmaker.firstName = [mmDict objectForKey:@"firstName"];
                         matchmaker.lastName = [mmDict objectForKey:@"lastName"];
                         matchmaker.iD = [mmDict objectForKey:@"id"];
                         matchmaker.imageLink = [mmDict objectForKey:@"image"];
                         matchmaker.countSingle = [mmDict objectForKey:@"countSingle"];
                         NSLog(@"%@ matchmakersForMatchmaker", mmDict);
                         [matchmakersArray addObject:matchmaker];
                     }
                     self.matchmakersForMatchmakerArray = matchmakersArray;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     NSData *dataFromArray = [NSKeyedArchiver archivedDataWithRootObject:self.matchmakersForMatchmakerArray];
                     [defaults setObject:dataFromArray forKey:@"matchmakersForMatchmakerArray"];
                     [defaults synchronize];
                     
                     // For MyNetworkViewController
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"networkReceived"
                                                                         object:self];
//                 });
             } failure:^(NSURLSessionTask *operation, NSError *error) {
                 [RESThelper errorDescription:error];
             }];
//    });
}

#pragma mark - Matchmakers for single
//2
- (void)matchmakersForSingle {
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^(void) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *accessToken = [defaults stringForKey:@"access_token"];
        NSString *completeURL = [NSString stringWithFormat:@"%@db/single/matchmakers?access_token=%@&&limit=%@&&offset=%@", base_url, accessToken, limit, offset];
        NSLog(@"%@ COMPLETE URL", completeURL);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = nil;
        [manager GET:completeURL
          parameters:nil
            progress:nil
             success:^(NSURLSessionTask *task, id responseObject) {
//                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSMutableArray *matchmakersArray = [NSMutableArray new];
                     Matchmaker *matchmaker = nil;
                     NSDictionary *responseData = [responseObject objectForKey:@"data"];
                     for (NSDictionary *mmDict in responseData) {
                         matchmaker = [Matchmaker new];
                         matchmaker.email = [mmDict objectForKey:@"email"];
                         matchmaker.firstName = [mmDict objectForKey:@"firstName"];
                         matchmaker.lastName = [mmDict objectForKey:@"lastName"];
                         matchmaker.iD = [mmDict objectForKey:@"id"];
                         matchmaker.imageLink = [mmDict objectForKey:@"image"];
                         matchmaker.countSingle = [mmDict objectForKey:@"countSingle"];
                         NSLog(@"%@ matchmakersForSingle", mmDict);
                         [matchmakersArray addObject:matchmaker];
                     }
                     self.matchmakersForSingleArray = matchmakersArray;
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     NSData *dataFromArray = [NSKeyedArchiver archivedDataWithRootObject:self.matchmakersForSingleArray];
                     [defaults setObject:dataFromArray forKey:@"matchmakersForSingleArray"];
                     [defaults synchronize];
                     
                     // For MyNetworkViewController
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"networkReceived"
                                                                         object:self];

//                 });
             } failure:^(NSURLSessionTask *operation, NSError *error) {
                 [RESThelper errorDescription:error];
             }];
//    });
}

@end
