//
//  MyNetworkContainerViewController.m
//  3Degrees
//
//  Created by Pavel on 13.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "MyNetworkContainerViewController.h"

@implementation MyNetworkContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Profile button pressed

- (void)profileButtonPressed:(id)sender {
    UIStoryboard *storyboard = self.storyboard;
    ProfileViewController *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profile"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:profileVC animated:NO completion:nil];
}

@end
