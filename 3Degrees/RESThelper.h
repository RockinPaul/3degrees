//
//  RESThelper.h
//  3Degrees
//
//  Created by Pavel on 05.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "Constants.h"
#import "SHA1Encoder.h"

@interface RESThelper : NSObject

+ (NSString *)errorDescription:(NSError *)error;
+ (NSString *)passwordWithSaltFromPassword:(NSString *)password;
+ (void)performRequest:(SEL)selector
             ForTarget:(id)target
        WithParameters:(NSDictionary *)params;
@end
