//
//  ProfileViewController.m
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import "ProfileViewController.h"

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.usernameLabel.text = [defaults stringForKey:@"userName"];
    NSString *age = [defaults stringForKey:@"age"];
    if (age.length > 0) {
        age = [age stringByAppendingString:@","];
    }
    NSString *location = [defaults stringForKey:@"location"];
    self.ageAndLocationLabel.text = [NSString stringWithFormat:@"%@ %@", age, location];

    NSData *avatarData = [defaults objectForKey:@"image"];
    NSData *bluredData = [defaults objectForKey:@"bluredImage"];
    self.avatarImageView.image = [UIImage imageWithData:avatarData];
    self.bluredImageView.image = [UIImage imageWithData:bluredData];
    
    self.generalItems = @[@"Edit My Profile", @"Invite Matchmakers", @"Invite Singles", @"Push Notifications", @"Log Out"];
    self.supportItems = @[@"Frequently Asked Questions", @"Contact Us"];
    self.aboutItems = @[@"Privacy Policy", @"Terms of Service"];
    
    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    
    [self generalButtonPressed:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"Cell";
    MenuItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[MenuItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.titleLabel.text = self.menuItems[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuItemCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell.titleLabel.text isEqualToString:@"Log Out"]) {
        [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout"
                                                            object:self];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (void)backButtonPressed:(id)sender {;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)generalButtonPressed:(id)sender {
    [self.generalButton setBackgroundImage:[UIImage imageNamed:@"button_menu_tab"] forState:UIControlStateNormal];
    [self.supportButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [self.aboutButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    self.menuItems = self.generalItems;
    [self.tableView reloadData];
}

- (void)supportButtonPressed:(id)sender {
    [self.generalButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [self.supportButton setBackgroundImage:[UIImage imageNamed:@"button_menu_tab"] forState:UIControlStateNormal];
    [self.aboutButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    self.menuItems = self.supportItems;
    [self.tableView reloadData];
}

- (void)aboutButtonPressed:(id)sender {
    [self.generalButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [self.supportButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
    [self.aboutButton setBackgroundImage:[UIImage imageNamed:@"button_menu_tab"] forState:UIControlStateNormal];
    self.menuItems = self.aboutItems;
    [self.tableView reloadData];
}

@end
