//
//  MenuItemCell.h
//  3Degrees
//
//  Created by Pavel on 24.12.15.
//  Copyright © 2015 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end
