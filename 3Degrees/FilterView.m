//
//  FilterView.m
//  3Degrees
//
//  Created by Pavel on 22.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "FilterView.h"

@implementation FilterView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    self.layer.borderColor = [ImageHandler colorWithHexString:@"e8e6e1"] .CGColor;
    self.layer.borderWidth = 1.0f;
}

- (void)setViewActive {
    UIImageView *frameView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 260, 60)];
    frameView.image = [UIImage imageNamed:@"border"];
    [self addSubview:frameView];
}

- (void)setViewInactive {
    for (UIView *view in [self subviews]) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
}

- (void)setViewText:(NSString *)text {
    self.textLabel.text = text;
}

@end
