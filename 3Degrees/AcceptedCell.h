//
//  AcceptedCell.h
//  3Degrees
//
//  Created by Pavel on 10.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcceptedCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *singleOneImageView;
@property (nonatomic, strong) IBOutlet UIImageView *singleTwoImageView;
@property (nonatomic, strong) IBOutlet UILabel *singleOneName;
@property (nonatomic, strong) IBOutlet UILabel *singleTwoName;
@property (nonatomic, strong) IBOutlet UILabel *text;
@property (nonatomic, strong) IBOutlet UILabel *singleOneDescription;
@property (nonatomic, strong) IBOutlet UILabel *singleTwoDescription;

@end
