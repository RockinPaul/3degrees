//
//  SingleDetailsViewController.m
//  3Degrees
//
//  Created by Pavel on 18.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "SingleDetailsViewController.h"


@implementation SingleDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.imageView = [[UIImageView alloc] initWithImage:self.coverImage];
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
