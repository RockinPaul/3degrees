//
//  ESDPContainerViewController.m
//  3Degrees
//
//  Created by Pavel on 14.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ESDPContainerViewController.h"

@interface ESDPContainerViewController ()

@end

@implementation ESDPContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *role = [defaults stringForKey:@"role"];
    if ([role isEqualToString:@"matchmaker"]) {
        self.exploreContainer.alpha = 1;
        self.proposalsContainer.alpha = 0;
    } else {
        self.exploreContainer.alpha = 0;
        self.proposalsContainer.alpha = 1;
    }
}

@end
