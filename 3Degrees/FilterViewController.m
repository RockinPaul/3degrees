//
//  FilterViewController.m
//  3Degrees
//
//  Created by Pavel on 21.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "FilterViewController.h"

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    float const width = self.view.frame.size.width;
    float const height = self.view.frame.size.height/2 - 100;
    float const leftPickerMargin = 0;
    float const rightPickerMargin = self.view.frame.size.width/2;
    float const topMargin = self.view.frame.size.height/2 + 100;
    
    self.views = [[NSMutableSet alloc] init];
    [self.views addObject: self.ageView];
    [self.views addObject: self.sexView];
    [self.views addObject: self.distanceView];
    [self.views addObject: self.matchmakerView];
    self.currentView = [FilterView new];
    
    for (FilterView *view in self.views) {
        UITapGestureRecognizer *recognizer =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(filterViewPressed:)];
        [view addGestureRecognizer:recognizer];
    }

    self.ageFromPicker = [UIPickerView new];
    self.ageToPicker = [UIPickerView new];
    self.sexPicker = [UIPickerView new];
    self.distancePicker = [UIPickerView new];
    self.matchmakerPicker = [UIPickerView new];
    self.currentPicker = [UIPickerView new];
    
    self.pickers = @[self.ageFromPicker, self.ageToPicker, self.sexPicker, self.distancePicker, self.matchmakerPicker];
    
    for (UIPickerView __strong *picker in self.pickers) {
        if ([picker isEqual:self.ageFromPicker]) {
            picker.frame = CGRectMake(leftPickerMargin, topMargin, width/2, height);
        } else if ([picker isEqual:self.ageToPicker]) {
            picker.frame = CGRectMake(rightPickerMargin, topMargin, width/2, height);
        } else {
            picker.frame = CGRectMake(leftPickerMargin, topMargin, width, height);
        }
        picker.delegate = self;
        picker.dataSource = self;
        picker.hidden = YES;
        picker.backgroundColor = [ImageHandler colorWithHexString:@"B5AD9F"];
        
        [self.view addSubview:picker];
    }

    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height/2 + 60, self.view.frame.size.width, 40)];
    self.titleView.backgroundColor = [ImageHandler colorWithHexString:@"C1C0BA"];
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(self.titleView.frame.size.width - 70, self.titleView.frame.size.height/2 - 15, 65, 30)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.titleView addSubview:doneButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.titleView.frame.size.width/2 - 50, self.titleView.frame.size.height/2 - 15, 100, 30)];
    
#warning - have to refactoring
//    titleLabel.text = @"Title";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.titleView addSubview:titleLabel];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, self.titleView.frame.size.height/2 - 15, 65, 30)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.titleView addSubview:cancelButton];
    
    [self.view addSubview:self.titleView];
    self.titleView.hidden = YES;
    
    NSMutableArray *ages = [NSMutableArray new];
    for (int i = 18; i < 100; i++) {
        [ages addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    SEL mmSelector = @selector(matchmakersListWithParameters:);
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjects:@[@"100", @"0"] forKeys:@[@"limit", @"offset"]];
    [RESThelper performRequest:mmSelector ForTarget:self WithParameters:parameters];
    
    self.currentArray = [NSArray new];
    self.ageArray = ages;
    self.sexArray = @[@"male", @"female"];
    self.distanceArray = @[@"25", @"50", @"75", @"100", @"150", @"200", @"250", @"300", @"350", @"400", @"450", @"500"];
    
    self.ageMin = @"";
    self.ageMax = @"";
    self.sex = @"";
    self.distance = @"";
    self.matchmakersId = @"";
}

#pragma mark - Filter button pressed

- (void)filterButtonPressed:(UIButton *)sender {
    
    NSDictionary *params = [[NSDictionary alloc]
                            initWithObjects:@[self.ageMax, self.ageMin, self.sex, self.distance, self.matchmakersId, @"0", @"100"]
                            forKeys:@[@"ageMax", @"ageMin", @"sex", @"distance", @"matchmakersId", @"offset", @"limit"]];
    
    FilterController *filterController = [FilterController sharedInstance];
    [filterController performRequestForSinglesWithParameters:params];

    // Back to explore singles screen
    CATransition* transition = [CATransition animation];
    transition.duration = 1.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionReveal; //kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal
    transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.view.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Filter view pressed

- (void)filterViewPressed:(FilterView *)sender {
    [self dismissPickersExcept:nil]; // Dismiss all pickers
    
    if ([[(UIGestureRecognizer *)sender view] isKindOfClass:[FilterView class]]) {
        FilterView *view = (FilterView *)[(UIGestureRecognizer *)sender view];
        [view setViewActive];
        self.currentView = view;
        for (FilterView *customView in self.views) {
            if (![customView isEqual:view]) {
                [customView setViewInactive];
            }
        }
        if ([view isEqual:self.ageView]) {
            self.ageFromPicker.hidden = NO;
//            if (self.ageFromPicker) self.ageFromPicker.hidden = !self.ageFromPicker.hidden;
            if (self.ageToPicker) self.ageToPicker.hidden = !self.ageToPicker.hidden;
//            [self dismissPickersExcept:@[self.ageFromPicker, self.ageToPicker]];
            self.currentPicker = self.ageFromPicker;
            self.currentArray = self.ageArray;
        } else if ([view isEqual:self.sexView]) {
            self.sexPicker.hidden = NO;
//            if (self.sexPicker) self.sexPicker.hidden = !self.sexPicker.hidden;
//            [self dismissPickersExcept:@[self.sexPicker]];
            self.currentPicker = self.sexPicker;
            self.currentArray = self.sexArray;
        } else if ([view isEqual:self.distanceView]) {
            self.distancePicker.hidden = NO;
//            if (self.distancePicker) self.distancePicker.hidden = !self.distancePicker.hidden;
            self.currentPicker = self.distancePicker;
            self.currentArray = self.distanceArray;
        } else if ([view isEqual:self.matchmakerView]) {
            self.matchmakerPicker.hidden = NO;
//            if (self.matchmakerPicker) self.matchmakerPicker.hidden = !self.matchmakerPicker.hidden;
            self.currentPicker = self.matchmakerPicker;
            self.currentArray = self.matchmakersArray;
            [self.matchmakerPicker reloadAllComponents];
        } else {
            NSLog(@"ERROR: WRONG VIEW RECOGNIZED");
        }
        self.titleView.hidden = NO;
    }
}

- (void)dismissPickersExcept:(NSArray *)pickers {
    for (UIPickerView *pickerView in self.pickers) {
        if (![pickers containsObject:pickerView]) {
            pickerView.hidden = YES;
        }
    }
}

#pragma mark - Matchmakers

// Wrapper for selector
- (void)matchmakersListWithParameters:(NSDictionary *)params {
    NSString *limit = [params objectForKey:@"limit"];
    NSString *offset = [params objectForKey:@"offset"];
    [self matchmakersListWithLimit:limit Offset:offset];
}

#pragma mark - Matchmakers list with limit/offset

- (void)matchmakersListWithLimit:(NSString *)limit
                        Offset:(NSString *)offset {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/matchmakers?access_token=%@&&limit=%@&&offset=%@", base_url, [defaults stringForKey:@"access_token"],  limit, offset];
    NSLog(@"%@ COMPLETE URL", completeURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    [manager GET:completeURL parameters:nil progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             NSDictionary *responseData = [responseObject objectForKey:@"data"];
             NSString *firstName = nil;
             NSString *lastName = nil;
             NSString *iD = nil;
             NSMutableArray *mmNames = [NSMutableArray new];
             NSMutableArray *mmIds = [NSMutableArray new];
             for (NSDictionary *mm in responseData) {
                 firstName = [mm objectForKey:@"firstName"];
                 lastName = [mm objectForKey:@"lastName"];
                 iD = [mm objectForKey:@"id"];
                 [mmNames addObject:[NSString stringWithFormat:@"%@ %@", firstName, lastName]];
                 [mmIds addObject:iD];
             }
             self.matchmakersArray = mmNames;
             self.matchmakersIdsArray = mmIds;
             [self.matchmakerPicker reloadAllComponents];
             
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
         }];
}


#pragma mark - UIPickerViewDataSource

//Columns in picker views

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
//Rows in each Column

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    if ([pickerView isEqual:self.ageFromPicker] || [pickerView isEqual:self.ageToPicker]) {
        return self.ageArray.count;
    } else if ([pickerView isEqual:self.sexPicker]) {
        return self.sexArray.count;
    } else if ([pickerView isEqual:self.distancePicker]) {
        return self.distanceArray.count;
    } else if ([pickerView isEqual:self.matchmakerPicker]){
        return self.matchmakersArray.count;
    } else {
        NSLog(@"ERROR: WRONG PICKER");
        return (NSInteger)[NSNull null];
    }
}

#pragma mark - UIPickerViewDelegate
#pragma mark - Attributed title for row for component

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = nil;
    if ([pickerView isEqual:self.ageFromPicker] || [pickerView isEqual:self.ageToPicker]) {
        title = [NSString stringWithFormat:@"%@", self.ageArray[row]];
    } else if ([pickerView isEqual:self.sexPicker]) {
        title = [NSString stringWithFormat:@"%@", self.sexArray[row]];
    } else if ([pickerView isEqual:self.distancePicker]) {
        title = [NSString stringWithFormat:@"%@", self.distanceArray[row]];
    } else {
        title = [NSString stringWithFormat:@"%@", self.matchmakersArray[row]];
    }
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}

#pragma mark - Title bar

- (void)doneButtonPressed:(UIButton *)sender {
    NSUInteger numComponents = [[self.currentPicker dataSource] numberOfComponentsInPickerView:self.currentPicker];
    NSString *text = nil;
    for (NSUInteger i = 0; i < numComponents; ++i) {
        NSUInteger selectedRow = [self.currentPicker selectedRowInComponent:i];
        NSString *title = self.currentArray[selectedRow];

        if ([self.currentPicker isEqual:self.ageFromPicker]) {
            NSUInteger selectedToRow = [self.ageToPicker selectedRowInComponent:i];
            NSString *toTitle = self.currentArray[selectedToRow];
            if ([toTitle integerValue] < [title integerValue]) {
                [self showAlert];
                text = @"Any"; // Back to initial state
            } else {
                text = [NSString stringWithFormat:@"%@ - %@", title, toTitle];
                self.ageMax = toTitle;
                self.ageMin = title;
            }
        } else {
            text = title;
            if ([self.currentPicker isEqual:self.sexPicker]) {
                self.sex = self.sexArray[selectedRow];
            } else if ([self.currentPicker isEqual:self.distancePicker]) {
                self.distance = self.distanceArray[selectedRow];
            } else if ([self.currentPicker isEqual:self.matchmakerPicker]) {
                if (self.matchmakersIdsArray != nil) {
                    self.matchmakersId = self.matchmakersIdsArray[selectedRow];
                }
            }
        }
        [self.currentView setViewText:text];
    }
    [self hidePicker];
}

- (void)cancelButtonPressed:(UIButton *)sender {
    [self dismissPickersExcept:nil];
    self.titleView.hidden = YES;
//    [self hidePicker];
}

#pragma mark - Alert

- (void)showAlert {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                    message:@"Please, select the correct range"
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Hide picker

- (void)hidePicker {
    self.currentPicker.hidden = YES;
    self.titleView.hidden = YES;
    if ([self.currentPicker isEqual:self.ageFromPicker]) {
        self.ageToPicker.hidden = YES;
    }
}

@end
