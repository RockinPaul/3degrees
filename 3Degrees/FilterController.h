//
//  FilterController.h
//  3Degrees
//
//  Created by Pavel on 11.02.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "Constants.h"
#import "Single.h"
#import "RESThelper.h"

@interface FilterController : NSObject

@property (nonatomic, strong) NSArray *singles;
@property (nonatomic) SEL applySelector;

- (void)performRequestForSinglesWithParameters:(NSDictionary *)params;
+ (FilterController *)sharedInstance;

@end
