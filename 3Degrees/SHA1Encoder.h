//
//  SHA1Encoder.h
//  3Degrees
//
//  Created by Pavel on 09.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface SHA1Encoder : NSObject

+ (NSString *)stringToSha1:(NSString *)hashkey;

@end
