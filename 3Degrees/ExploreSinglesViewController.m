//
//  ExploreSinglesViewController.m
//  3Degrees
//
//  Created by Pavel on 12.01.16.
//  Copyright © 2016 itrc. All rights reserved.
//

#import "ExploreSinglesViewController.h"

@implementation ExploreSinglesViewController


- (void)awakeFromNib
{
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
}

- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    self.topCarousel.delegate = nil;
    self.topCarousel.dataSource = nil;
    self.bottomCarousel.delegate = nil;
    self.bottomCarousel.dataSource = nil;
}

#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isInitialCase = YES; // INITIAL STATE INDICATOR (WE NEED THIS FOR PRIMARY INIT)
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateSingles)
                                                 name:@"applyFilterWithParameters"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateOwn)
                                                 name:@"requestWithoutParameters"
                                               object:nil];
    
    //configure carousel
    self.topCarousel.type = iCarouselTypeCustom;
    self.bottomCarousel.type = iCarouselTypeCustom;
    self.topCarousel.dataSource = self;
    self.topCarousel.delegate = self;
    self.bottomCarousel.dataSource = self;
    self.bottomCarousel.delegate = self;
    
    // REQUEST FOR ALL USER'S SINGLES (TOP CAROUSEL)
    [[FilterController sharedInstance] performRequestForSinglesWithParameters:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    //free up memory by releasing subviews
    self.topCarousel = nil;
    self.bottomCarousel = nil;
}

#pragma mark - Update feed 

- (void)updateSingles {
    self.singlesArray = [NSArray new];//@[@"Sam Lake", @"John Deere", @"Frank Lee", @"Raj Thakeray", @"Maggie McGill", @"Dina Ray", @"Patricia Luck", @"Alice Montgomery"];
    self.photosArray = [NSMutableArray new];// [@"explore1", @"explore2", @"explore3", @"explore4", @"explore5", @"explore6", @"explore7", @"explore8"];
    self.locationsArray = [NSMutableArray new];
    self.agesArray = [NSMutableArray new];
    self.usernamesArray = [NSMutableArray new];
    self.singlesIDsArray = [NSMutableArray new];
    
    self.singlesArray = [FilterController sharedInstance].singles;
    NSMutableArray *tempArray = [NSMutableArray new];
    for (int i = (int)self.singlesArray.count - 1; i >= 0; i--) {
        [tempArray addObject:self.singlesArray[i]];
    }
    self.singlesArray = (NSArray *)tempArray;
    
    for (Single *single in self.singlesArray) {
        [self.usernamesArray addObject:[NSString stringWithFormat:@"%@ %@", single.firstName, single.lastName]];
        [self.locationsArray addObject:single.location];
        [self.agesArray addObject:single.age];
        [self.singlesIDsArray addObject:single.iD];
        if (single.imageLink == nil) {
            [self.photosArray addObject:@"default"];
        } else {
            [self.photosArray addObject:single.imageLink];
        }
    }
    [self.bottomCarousel reloadData];
    [self carouselCurrentItemIndexDidChange:self.bottomCarousel]; // Update single info at bottom.
}

- (void)updateOwn {
    
    self.ownSinglesArray = [NSArray new];
    self.ownPhotosArray = [NSMutableArray new];
    self.ownLocationsArray = [NSMutableArray new];
    self.ownAgesArray = [NSMutableArray new];
    self.ownUsernamesArray = [NSMutableArray new];
    self.ownSinglesArray = [FilterController sharedInstance].singles;
    self.ownSinglesIDsArray = [NSMutableArray new];
    for (Single *single in self.ownSinglesArray) {
        [self.ownUsernamesArray addObject:[NSString stringWithFormat:@"%@ %@", single.firstName, single.lastName]];
        [self.ownLocationsArray addObject:single.location];
        [self.ownAgesArray addObject:single.age];
        [self.ownSinglesIDsArray addObject:single.iD];
        if (single.imageLink == nil) {
            [self.ownPhotosArray addObject:@"default"];
        } else {
            [self.ownPhotosArray addObject:single.imageLink];
        }
    }
    
    if (self.isInitialCase) {
        self.isInitialCase = NO;
        self.singlesArray = self.ownSinglesArray;
        [self updateSingles];
    }
    [self.topCarousel reloadData];
}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if ([carousel isEqual:self.topCarousel]) {
        return self.ownSinglesArray.count;
    } else {
        return self.singlesArray.count;
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *usernameLabel = nil;
    UILabel *locationLabel = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 230.0f)];
        view.tag = index;
        view.backgroundColor = [UIColor blackColor];
        view.contentMode = UIViewContentModeScaleToFill;
        if ([carousel isEqual:self.topCarousel]) {
//            image = [UIImage imageNamed:self.ownPhotosArray[index]];
            usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 18, 150, 30)];
            locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 40, 150, 30)];
        } else {
//            image = [UIImage imageNamed:self.photosArray[index]];
            usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 150, 150, 30)];
            locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 172, 150, 30)];
        }
//        ((UIImageView *)view).image = image;
        
        ((UIImageView *)view).image = [UIImage imageNamed:@"blank_explore"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            NSData *imageData = nil;
            if ([carousel isEqual:self.topCarousel]) {
                imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.ownPhotosArray[index]]];
            } else {
                imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.photosArray[index]]];
            }
            
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (view.tag == index) {
                        ((UIImageView *)view).image = image;
                    }
                });
            }
        });
        
        usernameLabel.alpha = 1;
        usernameLabel.textAlignment = NSTextAlignmentLeft;
        usernameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        usernameLabel.textColor = [UIColor whiteColor];
        
        locationLabel.alpha = 0.8;
        locationLabel.textAlignment = NSTextAlignmentLeft;
        locationLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
        locationLabel.textColor = [UIColor whiteColor];
        
        [view addSubview:usernameLabel];
        [view addSubview:locationLabel];
    }
    else
    {
        //get a reference to the label in the recycled view
        usernameLabel = (UILabel *)[view viewWithTag:index];
        ((UIImageView *)view).image = ((UIImageView *)[view viewWithTag:1]).image;
    }
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    if ([carousel isEqual:self.topCarousel]) {
        usernameLabel.text = self.ownUsernamesArray[index];
        locationLabel.text = self.ownLocationsArray[index];
    } else {
        usernameLabel.text = self.usernamesArray[index];
        locationLabel.text = self.locationsArray[index];
    }
    
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    if ([carousel isEqual:self.bottomCarousel]) {
        
    }
}

#pragma mark - Custom carousel set up

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    CGFloat MAX_SCALE = 1.0f; //max scale of center item
    CGFloat MAX_SHIFT = 1.0f; //amount to shift items to keep spacing the same
    
    CGFloat shift = fminf(1.0f, fmaxf(-1.0f, offset));
    CGFloat scale = 1.0f + (1.0f - fabs(shift)) * (MAX_SCALE - 1.0f);
    transform = CATransform3DTranslate(transform, offset * _carousel.itemWidth * 1.0f + shift * MAX_SHIFT, 0.0f, 0.0f);
    return CATransform3DScale(transform, scale, scale, scale);
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel {
    if ([carousel isEqual:self.topCarousel]) {
        int topIndex = (int)carousel.currentItemIndex;
        self.currentOwnSingleId = self.ownSinglesIDsArray[topIndex];
    } else {
        int bottomIndex = (int)carousel.currentItemIndex;
        self.currentSingleId = self.singlesIDsArray[bottomIndex];
        Single *single = self.singlesArray[bottomIndex];
        self.positionLabel.text = single.profession;
        self.jobLabel.text = single.job;
        self.degreeLabel.text = single.degreeTitle;
        self.institutionLabel.text = single.institution;
        self.bioTextView.text = single.bio;
    }
}

#pragma mark - Pair up

- (void)pairButtonPressed:(UIButton *)sender {
    [self pairUpSinglesWithId:self.currentOwnSingleId and:self.currentSingleId];
}

- (void)pairUpSinglesWithId:(NSString *)firstId
                        and:(NSString *)secondId  {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *accessToken = [defaults stringForKey:@"access_token"];
    NSString *completeURL = [NSString stringWithFormat:@"%@db/match?access_token=%@", base_url, accessToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = nil;
    NSLog(@"%@ COMPLETE URL", completeURL);
    [manager POST:completeURL parameters:@{
                                           @"singleFirst": firstId,
                                           @"singleSecond": secondId,
                                          }
         progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:2];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"pair_up"
                                                                 object:self];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [RESThelper errorDescription:error];
        }];
}

@end
